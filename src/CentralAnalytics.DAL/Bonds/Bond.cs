﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Text.Json;

namespace CentralAnalytics.DAL.Bonds;

public record Bond(
    long Id,
    string Figi,
    string Isin,
    string Ticker,
    string Currency,
    DateTime MaturityDate,
    DateTime PlacementDate,
    string CountryOfRiskName,
    string Sector,
    bool IsActive,
    string Payload,
    string Name,
    string Embedding)
{
    public static Bond FromTinkoffBond(Tinkoff.InvestApi.V1.Bond bond)
    {
        return new Bond(
            default,
            bond.Figi,
            bond.Isin,
            bond.Ticker,
            bond.Currency,
            bond.MaturityDate?.ToDateTime() ?? default,
            bond.PlacementDate?.ToDateTime() ?? default,
            bond.CountryOfRiskName,
            bond.Sector,
            true,
            JsonSerializer.Serialize(bond),
            bond.Name,
            default);
    }

    public Bond() : this(default, default, default, default, default, default, default, default, default, default, default, default, default)
    {
        
    }
}