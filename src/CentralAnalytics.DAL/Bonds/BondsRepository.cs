﻿using CentralAnalytics.DAL.Instrument;
using CentralAnalytics.Shared.Infrastructure.ConnectionFactory;
using Dapper;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace CentralAnalytics.DAL.Bonds;

public interface IBondsRepository
{
    Task<int> InsertBonds(
        IEnumerable<Bond> bonds, 
        CancellationToken ct);

    Task<IEnumerable<Bond>> GetAllBondsFromPostgres(
        CancellationToken ct);

    Task<int> UpdateEmbeddings(IEnumerable<Bond> bonds, CancellationToken ct);
}

public class BondsRepository : IBondsRepository
{
    private readonly IPgConnectionProvider _connectionProvider;
    private readonly List<Bond> _cachedBonds = new ();
    private readonly ILogger<BondsRepository> _logger;

    public BondsRepository(
        IPgConnectionProvider connectionProvider, 
        ILogger<BondsRepository> logger)
    {
        _connectionProvider = connectionProvider;
        _logger = logger;
    }
    
    public async Task<int> InsertBonds(IEnumerable<Bond> bonds, CancellationToken ct)
    {
        const string query = @"
                INSERT INTO instrument.bonds
                    (ticker, figi, isin, currency, maturity_date, placement_date, country_of_risk_name, sector, is_active, payload)
                 VALUES (@Ticker, @Figi, @Isin, @Currency, @MaturityDate, @PlacementDate, @CountryOfRiskName, @Sector, @IsActive, cast(@Payload as JSONB));";

        using var connection = _connectionProvider.GetDbConnection();
        
        var resp = 
            await connection.ExecuteAsync(
                new CommandDefinition(
                    query,
                    bonds,
                    cancellationToken: ct));

        return resp;
    }
    
    public async Task<int> UpdateEmbeddings(IEnumerable<Bond> bonds, CancellationToken ct)
    {
        const string query = @"
                UPDATE instrument.bonds as b SET 
                    country_of_risk_embedding = c.embedding
                    FROM(VALUES (@Id, cast(@Embedding as JSONB))) AS c(id, embedding)
                    WHERE c.id = b.id;";

        using var connection = _connectionProvider.GetDbConnection();
        
        var resp = 
            await connection.ExecuteAsync(
                new CommandDefinition(
                    query,
                    bonds.Select(x => new {x.Id, x.Embedding}),
                    cancellationToken: ct));

        return resp;
    }
    
    public async Task<IEnumerable<Bond>> GetAllBondsFromPostgres(CancellationToken ct)
    {
        if (_cachedBonds.Count > 0)
        {
            return _cachedBonds;
        }
        
        const string query = @"
                SELECT 
                    id,
                    ticker,
                    figi,
                    isin,
                    currency,
                    maturity_date as MaturityDate,
                    placement_date as PlacementDate,
                    country_of_risk_name CountryOfRiskName,
                    sector,
                    is_active as IsActive,
                    payload,
                    name,
                    country_of_risk_embedding Embedding
                 FROM instrument.bonds;";

        using var connection = _connectionProvider.GetDbConnection();
        
        var resp = 
            await connection.QueryAsync<Bond>(
                new CommandDefinition(
                    query,
                    null,
                    cancellationToken: ct));

        return resp;
    }
}