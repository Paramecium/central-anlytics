﻿using CentralAnalytics.Shared.Infrastructure.ConnectionFactory;
using Dapper;
using Microsoft.Extensions.Logging;

namespace CentralAnalytics.DAL.Dividend;

public interface IDividendRepository
{
    public Task<IEnumerable<Dividend>> GetDividendsByInstrumentId(long instrumentId, CancellationToken ct);
}

public class DividendRepository : IDividendRepository
{
    private readonly IPgConnectionProvider _connectionProvider;
    private readonly ILogger<DividendRepository> _logger;

    public DividendRepository(
        IPgConnectionProvider connectionProvider, 
        ILogger<DividendRepository> logger)
    {
        _connectionProvider = connectionProvider;
        _logger = logger;
    }
    
    public async Task<IEnumerable<Dividend>> GetDividendsByInstrumentId(long instrumentId, CancellationToken ct)
    {
        const string query = @"
                SELECT
                    id as Id,
                    instrument_id as InstrumentId, 
                    amount as Amount,
                    currency as Currency, 
                    register_closing_date as RegisterClosingDate, 
                    is_amount_confirmed as IsAmountConfirmed, 
                    is_register_closing_date_confirmed as IsRegisterClosingDateConfirmed, 
                    period as Period
                 FROM 
                     instrument.dividend
                WHERE instrument_id = @instrumentId;
               ";
        
        using var connection = _connectionProvider.GetDbConnection();
        
        var response = await connection.QueryAsync<Dividend>(new CommandDefinition(query, new {instrumentId}, cancellationToken: ct));
        

        return response.ToList();
    }
}