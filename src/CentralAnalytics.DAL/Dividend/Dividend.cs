﻿namespace CentralAnalytics.DAL.Dividend;

public record Dividend(long Id, long InstrumentId, double Amount, string Currency, DateTime? RegisterClosingDate,
    bool IsAmountConfirmed, bool IsRegisterClosingDateConfirmed, string Period)
{
    public Dividend(): this(default, default, default, default, default, default, default, default){}
}