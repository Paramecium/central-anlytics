namespace CentralAnalytics.DAL.WDAL;

public record SubscriptionRequest(
    IEnumerable<string> InstrumentsList,
    MarketDataSubscriptionAction Action);