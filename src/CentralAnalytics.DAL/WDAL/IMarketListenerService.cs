using Microsoft.Extensions.Hosting;

namespace CentralAnalytics.DAL.WDAL;

public interface IMarketListenerService
{
    MarketType MarketType { get; }

    public Task Subscribe(SubscriptionRequest subscriptionRequest, CancellationToken cancellationToken);
}