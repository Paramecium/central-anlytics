namespace CentralAnalytics.DAL.WDAL;

public enum MarketType
{
    Tinkoff = 0,
    Gate = 1
}