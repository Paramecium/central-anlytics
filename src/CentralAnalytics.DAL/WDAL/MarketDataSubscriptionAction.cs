namespace CentralAnalytics.DAL.WDAL;

public enum MarketDataSubscriptionAction
{
    Unknown = 0,
    Subscribe = 1,
    Unsubscribe = 2
}