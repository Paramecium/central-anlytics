namespace CentralAnalytics.DAL.WDAL;

public interface IMarketServiceProvider
{
    IMarketListenerService Get(MarketType marketType);
}