using System.Text.Json;
using System.Threading.Channels;
using CentralAnalytics.DAL.Instrument;
using Grpc.Core;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Tinkoff.InvestApi.V1;

namespace CentralAnalytics.DAL.WDAL.Tinkoff;

public class TinkoffMarketStreamHostedService: BackgroundService, IMarketListenerService
{
    private const int TimeToSleep = 300_000;
    private readonly ILogger<TinkoffMarketStreamHostedService> _logger;
    private readonly IInstrumentRepository _instrumentRepository;

    private readonly AsyncDuplexStreamingCall<MarketDataRequest, MarketDataResponse> _tinkoffMarketDataStream;

    private readonly ChannelWriter<TinkoffMarketStreamResponse> _tinkoffMarketStreamResponseWriter;

    public TinkoffMarketStreamHostedService(
        TinkoffClient.TinkoffClient client,
        ILogger<TinkoffMarketStreamHostedService> logger,
        IInstrumentRepository instrumentRepository,
        Channel<TinkoffMarketStreamResponse> tinkoffMarketStreamResponseChannel)
    {
        _logger = logger;
        _instrumentRepository = instrumentRepository;
        _tinkoffMarketDataStream = client.MarketDataStream();

        _tinkoffMarketStreamResponseWriter = tinkoffMarketStreamResponseChannel.Writer;
    }

    public MarketType MarketType { get; } = MarketType.Tinkoff;

    public override Task StartAsync(CancellationToken ct)
    {
        _logger.LogInformation("TinkoffMarketStreamHostedService Started");
        return base.StartAsync(ct);
    }

    protected override async Task ExecuteAsync(CancellationToken ct)
    {
        await Task.Yield();
        while (true)
        {
            try
            {
                if (_tinkoffMarketDataStream?.ResponseStream is null)
                {
                    await Task.Delay(TimeToSleep, ct);
                    continue;
                }

                await foreach (
                    var response
                    in _tinkoffMarketDataStream.ResponseStream.ReadAllAsync(ct))
                {
                    if (response?.LastPrice?.Figi == null)
                    {
                        continue;
                    }

                    var instruments =
                        await _instrumentRepository.GetInstrumentsByFigis(
                            new[] { response.LastPrice.Figi }, ct);

                    var instrument = instruments.First();

                    var marketLastTradePrice =
                        new FinInstrumentPrice(
                            instrument,
                            FromTinkoffQuotationToDecimal(response.LastPrice.Price),
                            response.LastPrice.Time.ToDateTimeOffset());

                    await _tinkoffMarketStreamResponseWriter.WriteAsync(
                        new TinkoffMarketStreamResponse(marketLastTradePrice), ct);
                }
            }
            catch (RpcException e) when (e.StatusCode == StatusCode.Cancelled)
            {
                _logger.LogInformation("Cancellation during Tinkoff stream listening requested.");
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.ToString());
            }

            await Task.Delay(TimeToSleep, ct);
        }
    }

    public override async Task StopAsync(CancellationToken ct)
    {
        await _tinkoffMarketDataStream.RequestStream.CompleteAsync();
        await base.StopAsync(ct);
    }

    public async Task Subscribe(SubscriptionRequest request, CancellationToken cancellationToken)
    {
        try
        {
            switch (request.Action)
            {
                case MarketDataSubscriptionAction.Subscribe:
                    await UpdateInstruments(SubscriptionAction.Subscribe, request.InstrumentsList, cancellationToken);
                    break;
                case MarketDataSubscriptionAction.Unsubscribe:
                    await UpdateInstruments(SubscriptionAction.Unsubscribe, request.InstrumentsList, cancellationToken);
                    break;
            }
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);
        }
    }

    private async Task UpdateInstruments(
        SubscriptionAction actionType,
        IEnumerable<string> figiList,
        CancellationToken cancellationToken)
        => await _tinkoffMarketDataStream.RequestStream.WriteAsync(
            new MarketDataRequest
            {
                SubscribeLastPriceRequest = new SubscribeLastPriceRequest
                {
                    SubscriptionAction = actionType,
                    Instruments =
                    {
                        ToTinkoffInstrument(figiList)
                    }
                }
            }, cancellationToken);

    private static IEnumerable<LastPriceInstrument> ToTinkoffInstrument(
        IEnumerable<string> figiList)
        => figiList.Select(x => new LastPriceInstrument { Figi = x });

    private static decimal FromTinkoffQuotationToDecimal(Quotation quotation)
        => quotation.Units + new decimal(quotation.Nano) / 1_000_000_000M;

    // private static IAsyncEnumerable<T> ReadAllAsync<T>(IAsyncStreamReader<T> streamReader,
    //     [EnumeratorCancellation] CancellationToken cancellationToken)
    // {
    //     return new List<T>();
    // }
}


public record FinInstrumentPrice(
    Instrument.Instrument FinInstrument,
    decimal Price,
    DateTimeOffset Timestamp)
{
    public override string ToString()
        => JsonSerializer.Serialize(this);
}
