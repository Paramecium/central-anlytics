namespace CentralAnalytics.DAL.WDAL.Tinkoff;

public record TinkoffMarketStreamResponse(FinInstrumentPrice LastPrice);