namespace CentralAnalytics.DAL.WDAL;

public class MarketListenerServiceProvider : IMarketServiceProvider
{
    private readonly Dictionary<MarketType, IMarketListenerService> _marketServices;
        
    public MarketListenerServiceProvider(IEnumerable<IMarketListenerService> marketServices)
    {
        _marketServices = marketServices.ToDictionary(x => x.MarketType);
    }

    public IMarketListenerService Get(MarketType engineType) => _marketServices[engineType];
}