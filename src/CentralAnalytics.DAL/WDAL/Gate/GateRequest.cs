﻿namespace CentralAnalytics.DAL.WDAL.Gate;

public record GateRequest(
    string Channel,
    string Event,
    string[] Payload,
    long Time);