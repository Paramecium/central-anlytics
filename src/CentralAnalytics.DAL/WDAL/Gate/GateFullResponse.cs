using System.Text.Json.Serialization;

namespace CentralAnalytics.DAL.WDAL.Gate;

public class GateFullResponse
{
    [JsonPropertyName("result")]
    private GateResponse Result { get; set; }
}