﻿using System.Text.Json;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Websocket.Client;

namespace CentralAnalytics.DAL.WDAL.Gate;

public class GateMarketStreamHostedService: IDisposable, IMarketListenerService, IHostedService
{
    private IWebsocketClient? _client;
    private readonly ILogger<GateMarketStreamHostedService> _logger;
    private readonly IOptionsMonitor<GateOptions> _gateOptions;

    public GateMarketStreamHostedService(
        ILogger<GateMarketStreamHostedService> logger,
        IOptionsMonitor<GateOptions> gateOptions)
    {
        _logger = logger;
        _gateOptions = gateOptions;
    }

    public MarketType MarketType { get; } = MarketType.Gate;

    public bool IsReady { get; private set; } = false;

    public async Task StartAsync(CancellationToken ct)
    {
        _logger.LogInformation("GateMarketStreamHostedService Started");
        await RegisterGate(ct);
        IsReady = true;
    }

    public Task StopAsync(CancellationToken ct)
    {
        return Task.CompletedTask;
    }

    public void Dispose()
    {
        _client?.Dispose();
    }

    private async Task RegisterGate(CancellationToken ct)
    {
        var options = _gateOptions.CurrentValue;
        var url = new Uri(options.ConnectionUri);
        _client = new WebsocketClient(url);
        _client.MessageReceived.Subscribe(msg =>
        {
            try
            {
                
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
            }
        });
        await _client.Start();
    }

    public Task Subscribe(SubscriptionRequest request, CancellationToken cancellationToken)
    {
        try
        {
            switch (request.Action)
            {
                case MarketDataSubscriptionAction.Subscribe:
                    UpdateInstruments("subscribe", request.InstrumentsList);
                    break;
                case MarketDataSubscriptionAction.Unsubscribe:
                    UpdateInstruments("unsubscribe", request.InstrumentsList);
                    break;
            }
        }
        catch (Exception e)
        {
            _logger.LogError(e, e.Message);
        }

        return Task.CompletedTask;
    }

    private void UpdateInstruments(string eventType, IEnumerable<string> instruments)
    {
        var options = _gateOptions.CurrentValue;
        foreach (var instrument in instruments)
        {
            var msg = new GateRequest(
                options.CandleChannel,
                eventType,
                new[] {options.Interval, $"{instrument}_{options.Currency}" },
                DateTimeOffset.Now.ToUnixTimeSeconds());

            _client?.Send(JsonSerializer.Serialize(msg));
        }
    }
}
