﻿using System.Text.Json.Serialization;

namespace CentralAnalytics.DAL.WDAL.Gate;

public class GateResponse
{
    [JsonPropertyName("t")]
    public long Timestamp { get; init; }
    
    [JsonPropertyName("v")]
    public double TotalVolume { get; init; }
    
    [JsonPropertyName("c")]
    public double ClosePrice { get; init; }
    
    [JsonPropertyName("h")]
    public double HighestPrice { get; init; }
    
    [JsonPropertyName("l")]
    public double LowestPrice { get; init; }

    [JsonPropertyName("o")]
    public double OpenPrice { get; init; }

    [JsonPropertyName("n")]
    public double SubscriptionName  { get; init; }
}