﻿namespace CentralAnalytics.DAL.WDAL.Gate;

public class GateOptions
{
    public string ConnectionUri { get; set; }
    
    public string CandleChannel { get; set; }
    
    public string Currency { get; set; }

    public string Interval { get; set; }
}