﻿using CentralAnalytics.Shared.Infrastructure.ConnectionFactory;
using Dapper;
using Microsoft.Extensions.Logging;

namespace CentralAnalytics.DAL.Decision;

public interface IDecisionRepository
{
    public Task InsertDecision(DecisionWithInstruments decisionWithInstruments, CancellationToken ct);
}

public class DecisionRepository : IDecisionRepository
{
    private readonly IPgConnectionProvider _connectionProvider;
    private readonly ILogger<DecisionRepository> _logger;

    public DecisionRepository(
        IPgConnectionProvider connectionProvider, 
        ILogger<DecisionRepository> logger)
    {
        _connectionProvider = connectionProvider;
        _logger = logger;
    }
    
    public async Task InsertDecision(
        DecisionWithInstruments decisionWithInstruments, 
        CancellationToken ct)
    {
        const string insertDecision = @"
                INSERT INTO decision.decision
                    (text, result, timestamp)
                VALUES (@Text, @Result, @Timestamp)
                RETURNING id;";

        using var connection = _connectionProvider.GetDbConnection();
        
        var decisionId = 
            await connection.QuerySingleAsync<int>(
                new CommandDefinition(
                    insertDecision,
                    new
                    {
                        decisionWithInstruments.Text,
                        decisionWithInstruments.Result,
                        Timestamp = DateTime.Now
                    },
                    cancellationToken: ct));

        if (decisionWithInstruments.InstrumentIds.Count > 0)
        {
            const string insertInstruments = @"
                INSERT INTO decision.decision_instruments
                    (decision_id, instrument_id)
                VALUES (@DecisionId, @InstrumentId);";

            await connection.ExecuteAsync(
                new CommandDefinition(insertInstruments,
                decisionWithInstruments.InstrumentIds.Select(x => new DecisionWithInstrumentId(decisionId, x)),
                cancellationToken: ct));
        }
        

        if (decisionWithInstruments.BondIds.Count > 0)
        {
            const string insertInstruments = @"
                INSERT INTO decision.decision_bonds
                    (decision_id, bond_id)
                VALUES (@DecisionId, @InstrumentId);";

            await connection.ExecuteAsync(
                new CommandDefinition(insertInstruments,
                    decisionWithInstruments.BondIds.Select(x => new DecisionWithInstrumentId(decisionId, x)),
                    cancellationToken: ct));
        }
    }


    private record DecisionWithInstrumentId(long DecisionId, long InstrumentId);
}