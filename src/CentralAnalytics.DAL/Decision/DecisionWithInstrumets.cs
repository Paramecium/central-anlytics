﻿namespace CentralAnalytics.DAL.Decision;

public class DecisionWithInstruments
{
    public long Id { get; set; }
    
    public string Text { get; set; }
    
    public int Result { get; set; }
    
    public List<long> InstrumentIds { get; set; }
    
    public List<long> BondIds { get; set; }
}