﻿using CentralAnalytics.Shared.Infrastructure.ConnectionFactory;
using Dapper;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace CentralAnalytics.DAL.CentralBank;

public interface ICentralBankRepository
{
    Task<IReadOnlyList<CentralBank>> GetAllCentralBanks();
}

public class CentralBankRepository : ICentralBankRepository
{
    private readonly IPgConnectionProvider _connectionProvider;
    private readonly Task<IReadOnlyList<CentralBank>> _centralBanks;
    private readonly ILogger<CentralBankRepository> _logger;

    public CentralBankRepository(
        IPgConnectionProvider connectionProvider, 
        ILogger<CentralBankRepository> logger)
    {
        _connectionProvider = connectionProvider;
        _logger = logger;
        _centralBanks = GetAllCentralBanksInternal();
    }

    public Task<IReadOnlyList<CentralBank>> GetAllCentralBanks()
    {
        return _centralBanks;
    }
    
    public async Task<IReadOnlyList<CentralBank>> GetAllCentralBanksInternal()
    {
        const string query = @"
                SELECT
                    id as Id,
                    name as Name, 
                    country_id as CountryId
                 FROM 
                     country.central_bank;
               ";


        using var connection = _connectionProvider.GetDbConnection();
        
        var response = await connection.QueryAsync<CentralBank>(new CommandDefinition(query));
        

        return response.ToList();
    }
}