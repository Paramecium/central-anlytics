﻿namespace CentralAnalytics.DAL.CentralBank;

public record CentralBank(
    long Id,
    string Name,
    string CountryId)
{
    
}