using CentralAnalytics.DAL.Instrument.GetAllActiveInstruments;
using CentralAnalytics.Shared.Infrastructure.ConnectionFactory;
using Dapper;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace CentralAnalytics.DAL.Instrument;

public interface IInstrumentRepository
{
    Task Reinitialize(CancellationToken ct);
    
    Task ActivateInstrumentsByFigis(
        IReadOnlyList<string> figis, 
        CancellationToken ct);
    
    Task DeactivateInstrumentsByFigis(
        IReadOnlyList<string> figis,
        CancellationToken ct);
    
    Task<IReadOnlyList<Instrument>> GetInstrumentsByFigis(
        IReadOnlyList<string> instrumentFigiList, 
        CancellationToken ct);
    
    Task<IReadOnlyList<Instrument>> GetAllActiveInstrumentsFromPostgres(
        CancellationToken ct);
    
    Task<int> UpdateEmbeddings(
        IEnumerable<Instrument> instruments,
        CancellationToken ct);
}

public class InstrumentRepository: IInstrumentRepository
{
    private readonly IPgConnectionProvider _connectionProvider;
    private readonly IMemoryCache _cachedInstruments;
    private readonly ILogger<InstrumentRepository> _logger;

    public InstrumentRepository(
        IPgConnectionProvider connectionProvider, 
        IMemoryCache cachedInstruments,
        ILogger<InstrumentRepository> logger)
    {
        _connectionProvider = connectionProvider;
        _cachedInstruments = cachedInstruments;
        _logger = logger;
    }
    
    public async Task Reinitialize(CancellationToken ct)
    {
        var allInstruments = await GetAllInstrumentsFromPostgres(ct);
        foreach (var instrument in allInstruments)
        {
            _cachedInstruments.Set(instrument.Figi, instrument);
        }
    }

    public async Task ActivateInstrumentsByFigis(
        IReadOnlyList<string> figis, 
        CancellationToken ct)
    {
        const string query = @"
                UPDATE
                    instrument.instrument ii
                SET
                    is_activate = true
                WHERE
                   ii.figi in :figis;
               ";

        using var connection = _connectionProvider.GetDbConnection();
        
        var resp = 
            await connection.ExecuteAsync(
                new CommandDefinition(
                    query,
                    new { figis },
                    cancellationToken: ct));

        foreach (var figi in figis)
        {
            if(_cachedInstruments.TryGetValue(figi, out Instrument? instrument))
            {
                _cachedInstruments.Set(figi, instrument! with { IsActive = true });
            }
        }
    }

    public async Task DeactivateInstrumentsByFigis(
        IReadOnlyList<string> figis, 
        CancellationToken ct)
    {
        const string query = @"
                UPDATE
                    instrument.instrument ii
                SET
                    is_activate = false
                WHERE
                   ii.figi in :figis;
               ";

        using var connection = _connectionProvider.GetDbConnection();
        
        var resp = 
            await connection.QueryAsync<Instrument>(
                new CommandDefinition(
                    query,
                    new { figis },
                    cancellationToken: ct));

        foreach (var figi in figis)
        {
            if(_cachedInstruments.TryGetValue(figi, out Instrument? instrument))
            {
                _cachedInstruments.Set(figi, instrument! with { IsActive = false });
            }
        }
    }

    public async Task<IReadOnlyList<Instrument>> GetInstrumentsByFigis(
        IReadOnlyList<string> instrumentFigiList, 
        CancellationToken ct)
    {
        var list = instrumentFigiList
            .Select(
                figi => (
                    Found: _cachedInstruments.TryGetValue(figi, out Instrument? instrument), 
                    Instrument: instrument,
                    Figi: figi))
            .ToList();

        var notFound =
            list
                .Where(x => x.Found is false)
                .Select(x => x.Figi)
                .ToList();
        
        // TODO Обработать отсутствие значений в меморикеше
        if (notFound.Count is not 0)
        {
            _logger.LogWarning($"INSTRUMENTS WERE NOT FOUND {string.Join(',', notFound)}");
        }

        return list.Select(x => x.Instrument).ToList()!;
    }
    
    private async Task<IReadOnlyList<Instrument>> GetAllInstrumentsFromPostgres(
        CancellationToken ct)
    {
        const string query = @"
                SELECT
                    id as Id,
                    name as Name, 
                    ticker as Ticker, 
                    figi as Figi,  
                    currency as Currency,
                    is_active as IsActive,
                    name_embedding as Embedding
                 FROM 
                     instrument.instrument ii;
               ";

        using var connection = _connectionProvider.GetDbConnection();
        
        var resp = 
            await connection.QueryAsync<Instrument>(
                new CommandDefinition(
                    query,
                    null,
                    cancellationToken: ct));

        return resp.ToList();
    }
    
    public async Task<IReadOnlyList<Instrument>> GetAllActiveInstrumentsFromPostgres(
        CancellationToken ct)
    {
        const string query = @"
                SELECT
                    id as Id,
                    name as Name, 
                    ticker as Ticker, 
                    figi as Figi,  
                    currency as Currency,
                    is_active as IsActive,
                    name_embedding as Embedding
                 FROM 
                     instrument.instrument ii
                 WHERE is_active = true;
               ";

        using var connection = _connectionProvider.GetDbConnection();
        
        var resp = 
            await connection.QueryAsync<Instrument>(
                new CommandDefinition(
                    query,
                    null,
                    cancellationToken: ct));

        return resp.ToList();
    }

    public async Task<int> UpdateEmbeddings(IEnumerable<Instrument> instruments, CancellationToken ct)
    {
        const string query = @"
                UPDATE instrument.instrument as ii SET 
                    name_embedding = c.embedding
                    FROM(VALUES (@Id, cast(@Embedding as JSONB))) AS c(id, embedding)
                    WHERE c.id = ii.id;";

        using var connection = _connectionProvider.GetDbConnection();
        
        var resp = 
            await connection.ExecuteAsync(
                new CommandDefinition(
                    query,
                    instruments.Select(x => new {x.Id, x.Embedding}),
                    cancellationToken: ct));

        return resp;
    }
}