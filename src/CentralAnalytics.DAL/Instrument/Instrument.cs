namespace CentralAnalytics.DAL.Instrument;

public record Instrument(
    long Id,
    string Name,
    string Ticker,
    string Figi,
    string Currency,
    bool IsActive,
    string Embedding)
{
    private Instrument() : this(
        default,default,default,
        default,default,default, default)
    {
        
    }
}