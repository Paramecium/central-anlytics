namespace CentralAnalytics.DAL.Instrument.GetAllActiveInstruments;

public record GetAllActiveInstrumentsResponse(
    IReadOnlyList<Instrument> InstrumentList);