namespace CentralAnalytics.Shared.Models;

public enum FilteredNewsType
{
    Fact = 1,
    Experience = 2,
    Opinion = 3,
    Other = 4
}