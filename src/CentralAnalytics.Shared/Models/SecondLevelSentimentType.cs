namespace CentralAnalytics.Shared.Models;

public enum SecondLevelSentimentType
{
    CorporateReport = 0,
    CorporateAdvice = 1,
    CryptoOther = 2,
    CorporateOther = 3,
    CorporateDividend = 4,
    EconomicalOther = 5,
    EconomicalStatistics = 6,
    PoliticalOther = 7,
    EconomicalCBRate = 8,
    Other = 9,
    EconomicalRegulations = 10,
    CorporatePriceChange = 11,
    CorporateBuyback = 12,
    CorporateDelisting = 13,
    CorporateExtraEmission = 14
}