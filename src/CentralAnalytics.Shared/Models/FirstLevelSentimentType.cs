namespace CentralAnalytics.Shared.Models;

public enum FirstLevelSentimentType
{
    Corporate = 0,
    Economical = 1,
    Other = 2,
    Political = 3,
    Crypto = 4
}