namespace CentralAnalytics.Shared.Models;

public record Pagination(
    int Limit,
    int Offset);
