using System.Text.Json.Serialization;

namespace CentralAnalytics.Shared.Models;

public sealed record AnalyzedArticle(
    long SourceId,
    string Summary,
    [property: JsonConverter(typeof(JsonStringEnumConverter))]
    FilteredNewsType FilterType,
    [property: JsonConverter(typeof(JsonStringEnumConverter))]
    FirstLevelSentimentType MainType,
    double MainTypeProbability,
    [property: JsonConverter(typeof(JsonStringEnumConverter))]
    SecondLevelSentimentType SecondType,
    double SecondTypeProbability,
    string Sentiment,
    double SentimentProbability,
    IReadOnlyList<string> Words,
    IReadOnlyList<string> Tags,
    DateTimeOffset TimeStamp,
    IReadOnlyList<string> Tokens)
{
    public AnalyzedArticle() : this(default, default, default, default, default, default, default, default, default, default, default, default, default){}
}