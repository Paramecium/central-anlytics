namespace CentralAnalytics.Shared.Models;

public sealed record NewsArticle(
    long SourceId,
    string Title,
    string? Summary,
    DateTimeOffset Timestamp,
    string[] Tags);