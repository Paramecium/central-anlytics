using Prometheus;

namespace CentralAnalytics.Shared.Infrastructure.Metrics;

public interface IInstrumentMetrics
{
    void InstrumentLastPriceReceivedInc(string ticker);
    void InstrumentLastPriceLog(string ticker, double price);

}

public class InstrumentMetrics : IInstrumentMetrics
{
    private const string ServiceName = "central_analytics";
    
    private const string InstrumentLastPriceReceivedCounterName = ServiceName + "_instrument_last_price_received";
    private const string InstrumentLastPriceReceivedCounterHelp = "last price event count";
    

    private readonly string[] _instrumentLastPriceReceivedLabels =
    {
        "instrument",
    };

    private readonly Counter _instrumentLastPriceReceived;
    
    
    private const string InstrumentLastPriceGaugeName = ServiceName + "_instrument_last_price";
    private const string InstrumentLastPriceGaugeHelp = "last prices gauge";
    
    private readonly string[] _instrumentLastPriceGaugeLabels =
    {
        "instrument",
    };
    
    private readonly Gauge _instrumentLastPrice;


    public InstrumentMetrics()
    {
        _instrumentLastPriceReceived = Prometheus.Metrics.CreateCounter(
            InstrumentLastPriceReceivedCounterName,
            InstrumentLastPriceReceivedCounterHelp,
            _instrumentLastPriceReceivedLabels);
        
        _instrumentLastPrice = Prometheus.Metrics.CreateGauge(
            InstrumentLastPriceGaugeName,
            InstrumentLastPriceGaugeHelp,
            _instrumentLastPriceGaugeLabels);
    }

    public void InstrumentLastPriceReceivedInc(
        string ticker)
    {
        _instrumentLastPriceReceived
            .WithLabels(ticker)
            .Inc();
    }
    
    public void InstrumentLastPriceLog(
        string ticker, double price)
    {
        _instrumentLastPrice
            .WithLabels(ticker)
            .Set(price);
    }
}