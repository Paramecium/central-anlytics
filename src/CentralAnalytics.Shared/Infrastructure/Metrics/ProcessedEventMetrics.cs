﻿using System.Diagnostics.Metrics;
using CentralAnalytics.Shared.Models;
using Prometheus;

namespace CentralAnalytics.Shared.Infrastructure.Metrics;

public interface IProcessedMessagesMetrics
{
    void ProcessedMessagesCountInc(AnalyzedArticle article);
}

public class ProcessedMessagesMetrics : IProcessedMessagesMetrics
{
    private const string ServiceName = "central_analytics";
    
    private const string ProcessedEventsCounterName = ServiceName + "_processed_messages";
    private const string InstrumentLastPriceReceivedCounterHelp = "number of news events processed";
    
    private readonly string[] _processedMessagesLabels =
    {
        "filter_type",
        "main_type",
        "second_type"
    };
    
    private readonly Counter _processedMessages;
    
    public ProcessedMessagesMetrics()
    {
        _processedMessages = Prometheus.Metrics.CreateCounter(
            ProcessedEventsCounterName,
            InstrumentLastPriceReceivedCounterHelp,
            _processedMessagesLabels);
    }
    
    public void ProcessedMessagesCountInc(AnalyzedArticle article)
    {
        _processedMessages.WithLabels(article.FilterType.ToString(), article.MainType.ToString(), article.SecondType.ToString()).Inc();
    }
}