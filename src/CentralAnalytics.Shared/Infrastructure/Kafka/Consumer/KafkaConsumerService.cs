using CentralAnalytics.Shared.Infrastructure.Kafka.KafkaEvent;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace CentralAnalytics.Shared.Infrastructure.Kafka.Consumer;

public class KafkaConsumerService<TSettings> : BackgroundService
    where TSettings : IKafkaEventsConsumerSettings
{
    private const int TimeoutMilliseconds = 1000;
    private readonly IKafkaConsumer<TSettings> _kafkaConsumer;
    private readonly ILogger<KafkaConsumerService<TSettings>> _logger;

    public KafkaConsumerService(
        IKafkaConsumer<TSettings> kafkaConsumer,
        ILogger<KafkaConsumerService<TSettings>> logger)
    {
        _kafkaConsumer = kafkaConsumer;
        _logger = logger;
    }

    protected override async Task ExecuteAsync(
        CancellationToken stoppingToken)
    {
        // внезапно https://github.com/dotnet/runtime/issues/36063
        await Task.Yield();

        while (!stoppingToken.IsCancellationRequested)
        {
            var isConsumed = false;
            try
            {
                isConsumed = await _kafkaConsumer.ConsumeNext(stoppingToken);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error during processing kafka message");
            }

            if (!isConsumed)
            {
                _logger.LogDebug($"No result from kafka, sleeping for {TimeoutMilliseconds} ms");
                await Task.Delay(TimeoutMilliseconds, stoppingToken);
            }
        }
    }

    public override Task StartAsync(
        CancellationToken cancellationToken)
    {
        _kafkaConsumer.Subscribe();
        return base.StartAsync(cancellationToken);
    }

    public override Task StopAsync(
        CancellationToken cancellationToken)
    {
        _kafkaConsumer.Unsubscribe();
        return base.StopAsync(cancellationToken);
    }
}
