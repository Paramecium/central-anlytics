﻿using CentralAnalytics.Shared.Infrastructure.Kafka.Deserializers;
using CentralAnalytics.Shared.Infrastructure.Kafka.KafkaEvent;
using Confluent.Kafka;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace CentralAnalytics.Shared.Infrastructure.Kafka.Consumer;

public class KafkaConsumer<TSettings> : IKafkaConsumer<TSettings>
    where TSettings : class, IKafkaEventsConsumerSettings
{
    private readonly IServiceProvider _serviceProvider;
    private readonly IKafkaEventTypeParser _parser;
    private readonly ILogger<KafkaConsumer<TSettings>> _logger;
    private readonly TSettings _settings;
    private IConsumer<Ignore, string>? _consumer;

    private readonly IDictionary<string, IKafkaEventDescription> _eventDescriptions =
        new Dictionary<string, IKafkaEventDescription>();

    public KafkaConsumer(
        IServiceProvider serviceProvider,
        IKafkaEventTypeParser parser,
        ILogger<KafkaConsumer<TSettings>> logger,
        IOptions<TSettings> settings,
        IReadOnlyList<IKafkaEventDescription> messageDescriptions)
    {
        _serviceProvider = serviceProvider;
        _logger = logger;
        _parser = parser;
        _settings = settings.Value;

        foreach (var description in messageDescriptions)
        {
            _eventDescriptions.Add(description.EventType.Name, description);
        }
    }

    public void Subscribe()
    {
        _logger.LogInformation($"Trying to subscribe on the topic {_settings.Topic}");
        _consumer = NewConsumer();
        _consumer.Subscribe(_settings.Topic);
        _logger.LogInformation($"Successfully subscribed on the topic {_settings.Topic}");
    }

    public void Unsubscribe()
    {
        _logger.LogInformation($"Trying to unsubscribe from the topic {_settings.Topic}");
        _consumer?.Unsubscribe();
        _consumer?.Dispose();
        _logger.LogInformation($"Successfully unsubscribed from the topic {_settings.Topic}");
    }

    private IConsumer<Ignore, string> NewConsumer()
    {
        var consumer = new ConsumerBuilder<Ignore, string>(_settings.ConsumerConfig)
            .SetErrorHandler(
                (_, error) =>
                {
                    _logger.LogError($"Error: {error.Reason}");
                })
            .SetOffsetsCommittedHandler(
                (_, offsets) =>
                {
                    _logger.LogDebug($"Commited offsets: {string.Join(", ", offsets.Offsets)}");
                })
            .SetStatisticsHandler(
                (_, json) =>
                {
                    _logger.LogDebug($"Statistics: {json}");
                })
            .SetPartitionsAssignedHandler(
                (_, partitions) =>
                {
                    _logger.LogInformation($"Assigned partitions: [{string.Join(", ", partitions)}]");
                })
            .SetPartitionsRevokedHandler(
                (_, partitions) =>
                {
                    _logger.LogInformation($"Revoking assignment: [{string.Join(", ", partitions)}]");
                })
            .Build();
        return consumer;
    }

    public async Task<bool> ConsumeNext(
        CancellationToken token)
    {
        _logger.LogDebug($"Consume next from {_settings.Topic}");

        // Даже при включенном EnablePartitionEof, Consume всё равно будет крутится в холостую.
        // Но можно явно задать время ожидания сообщения из Kafka,
        // тогда Consume будет "прокручиваться" только один раз,
        // а мы потом уже освободим тред, если ничего не пришло.
        var consumeResult = _consumer?.Consume(_settings.ConsumeTimeoutMilliseconds);
        //_consumer?.Commit(new [] {new TopicPartitionOffset (new TopicPartition("analyzed-articles-test", new Partition(0)), new Offset(700)) });
        if (consumeResult == null || consumeResult.IsPartitionEOF)
        {
            _logger.LogDebug($"Nothing to consume from {_settings.Topic}");
            return false;
        }

        var messageType = _parser.ResolveEventType(consumeResult);

        if (messageType is null)
        {
            _logger.LogWarning($"Unknown message without type header\r\n{consumeResult.Message.Value}");

            // Делаем StoreOffset с автокоммитом.
            // https://docs.confluent.io/clients-confluent-kafka-dotnet/current/overview.html#auto-offset-commit
            _consumer?.StoreOffset(consumeResult);
            return true;
        }

        if (!_eventDescriptions.TryGetValue(messageType, out var messageDescription))
        {
            _logger.LogWarning($"Can't find handler for message with type '{messageType}'");
        }
        else
        {
            _logger.LogDebug($"Processing message {messageType} from {_settings.Topic}");
            await ProcessMessage(messageDescription, consumeResult, token);
        }

        // Делаем StoreOffset с автокоммитом.
        // https://docs.confluent.io/clients-confluent-kafka-dotnet/current/overview.html#auto-offset-commit
        _consumer?.StoreOffset(consumeResult);
        return true;
    }

    private async Task ProcessMessage(
        IKafkaEventDescription eventDescription,
        ConsumeResult<Ignore, string> consumeResult,
        CancellationToken cancellationToken)
    {
        var messageValue = consumeResult.Message.Value;

        try
        {
            var handler = GetRequiredService<IKafkaEventHandler>(eventDescription.EventHandlerInterface);
            var deserializer = GetRequiredService<IKafkaDeserializer>(eventDescription.EventDeserializerType);
            var message = deserializer.Deserialize(messageValue, eventDescription.EventType);
            if (message is null)
            {
                return;
            }
            await handler.Handle(message, cancellationToken);
        }
        catch (Exception e)
        {
            _logger.LogError(e, $"Poison message of {eventDescription.EventType},\r\n{messageValue}");
        }
    }

    private TService GetRequiredService<TService>(
        Type serviceType)
    {
        try
        {
            return (TService) _serviceProvider.GetRequiredService(serviceType);
        }
        catch (Exception e)
        {
            var message = $"Can't find handler for type '({typeof(TService)}) {serviceType}'";
            _logger.LogError(e, message);
            throw;
        }
    }

    public void Dispose()
    {
        _consumer?.Unsubscribe();
        _consumer?.Dispose();
    }
}
