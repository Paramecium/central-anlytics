using CentralAnalytics.Shared.Infrastructure.Kafka.Deserializers;
using CentralAnalytics.Shared.Infrastructure.Kafka.KafkaEvent;

namespace CentralAnalytics.Shared.Infrastructure.Kafka.Consumer;

public class KafkaConsumerBuilder
{
    public ICollection<IKafkaEventDescription> Descriptions { get; } = new List<IKafkaEventDescription>();

    public KafkaConsumerBuilder WithHandler<TEvent, TEventHandler, TDeserializer>()
        where TEvent : class, new()
        where TEventHandler : class, IKafkaEventHandler<TEvent>
        where TDeserializer : class, IKafkaDeserializer
    {
        Descriptions.Add(
            new KafkaEventDescription(
                typeof(TEvent),
                typeof(TEventHandler),
                typeof(TDeserializer)));
        return this;
    }
}
