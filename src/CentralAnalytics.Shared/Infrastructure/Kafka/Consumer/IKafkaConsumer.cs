﻿using CentralAnalytics.Shared.Infrastructure.Kafka.KafkaEvent;

namespace CentralAnalytics.Shared.Infrastructure.Kafka.Consumer;

public interface IKafkaConsumer<T> : IDisposable
    where T : IKafkaEventsConsumerSettings
{
    void Subscribe();
    void Unsubscribe();

    Task<bool> ConsumeNext(
        CancellationToken token);
}
