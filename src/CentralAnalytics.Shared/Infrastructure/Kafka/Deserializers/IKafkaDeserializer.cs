﻿namespace CentralAnalytics.Shared.Infrastructure.Kafka.Deserializers;

public interface IKafkaDeserializer
{
    object? Deserialize(
        string obj,
        Type objType);
}
