﻿using System.Text.Json;
using System.Text.RegularExpressions;
using Microsoft.Extensions.Logging;

namespace CentralAnalytics.Shared.Infrastructure.Kafka.Deserializers;

public class JsonKafkaDeserializer : IKafkaDeserializer
{
    private readonly ILogger<JsonKafkaDeserializer> _logger;

    public JsonKafkaDeserializer(
        ILogger<JsonKafkaDeserializer> logger)
    {
        _logger = logger;
    }

    public object? Deserialize(
        string obj,
        Type objType)
    {
        //const string patternFixed = @"'(?=\w.*')|'(?=[:,}])";
        //obj = Regex.Replace(obj, patternFixed, "\"");

        try
        {
            return JsonSerializer.Deserialize(obj, objType, new JsonSerializerOptions());
        }
        catch (Exception ex)
        {
            _logger.LogError(
                ex,
                $"Deserialization error {ex.Message}: message '{obj}'");
            return null;
        }

    }
}
