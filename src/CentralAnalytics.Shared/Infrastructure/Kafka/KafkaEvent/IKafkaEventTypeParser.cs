﻿using Confluent.Kafka;

namespace CentralAnalytics.Shared.Infrastructure.Kafka.KafkaEvent;

public interface IKafkaEventTypeParser
{
    string ResolveEventType(
        ConsumeResult<Ignore, string> consumeResult);
}
