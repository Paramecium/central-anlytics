namespace CentralAnalytics.Shared.Infrastructure.Kafka.KafkaEvent;

public interface IKafkaEventDescription
{
    /// <summary>
    /// Тип объекта, к которому десериализуется событие Kafka
    /// </summary>
    Type EventType { get; }

    /// <summary>
    /// Тип обработчика события Kafka
    /// </summary>
    Type EventHandlerType { get; }

    /// <summary>
    /// Интерфейс обработчика события Kafka. Необходим для получения экземпляра из DI контейнера
    /// </summary>
    Type EventHandlerInterface { get; }

    /// <summary>
    /// Тип десериализатора содержимого события Kafka
    /// </summary>
    Type EventDeserializerType { get; }
}

public class KafkaEventDescription : IKafkaEventDescription
{
    public Type EventType { get; }

    public Type EventHandlerType { get; }

    public Type EventDeserializerType { get; }

    public Type EventHandlerInterface { get; }

    public KafkaEventDescription(
        Type messageType,
        Type messageHandlerType,
        Type messageDeserializerType)
    {
        EventType = messageType;
        EventHandlerType = messageHandlerType;
        EventDeserializerType = messageDeserializerType;
        EventHandlerInterface = typeof(IKafkaEventHandler<>).MakeGenericType(EventType);
    }
}
