using Microsoft.AspNetCore.Rewrite;

namespace CentralAnalytics.Shared.Infrastructure.Kafka.KafkaEvent;

public interface IKafkaEventHandler
{
    Task Handle(
        object message,
        CancellationToken ct);
}

public interface IKafkaEventHandler<TMessage> : IKafkaEventHandler
{
    Task IKafkaEventHandler.Handle(
        object message,
        CancellationToken ct)
    {
        return Handle((TMessage) message, ct);
    }

    Task Handle(
        TMessage message,
        CancellationToken ct);
}
