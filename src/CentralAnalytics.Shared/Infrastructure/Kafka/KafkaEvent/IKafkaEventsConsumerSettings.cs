﻿using Confluent.Kafka;

namespace CentralAnalytics.Shared.Infrastructure.Kafka.KafkaEvent;

public interface IKafkaEventsConsumerSettings
{
    ConsumerConfig? ConsumerConfig { get; set; }

    int ConsumeTimeoutMilliseconds { get; set; }

    string? Topic { get; set; }
}
