using System.Data;
using CentralAnalytics.Shared.Infrastructure.Configuration;
using Microsoft.Extensions.Options;
using Npgsql;

namespace CentralAnalytics.Shared.Infrastructure.ConnectionFactory;

public sealed class PgConnectionProvider : IPgConnectionProvider
{
    private readonly string _connectionString;

    public PgConnectionProvider(IOptions<PgConnectionOptions> dbOptions)
    {
        _connectionString = dbOptions.Value.ConnectionString;
    }

    public IDbConnection GetDbConnection()
        => new NpgsqlConnection(_connectionString);
}
