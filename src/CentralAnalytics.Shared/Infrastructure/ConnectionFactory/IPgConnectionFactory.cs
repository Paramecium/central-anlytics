using System.Data;

namespace  CentralAnalytics.Shared.Infrastructure.ConnectionFactory;

public interface IPgConnectionProvider
{
    IDbConnection GetDbConnection();
}
