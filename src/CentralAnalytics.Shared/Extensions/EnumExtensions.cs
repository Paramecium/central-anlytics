using System.ComponentModel;
using Microsoft.OpenApi.Extensions;

namespace CentralAnalytics.Shared.Extensions;

public static class EnumExtensions
{
    public static string GetDescription<T>(this T type) where T : Enum
        => type.GetAttributeOfType<DescriptionAttribute>()?.Description ?? type.ToString();
}
