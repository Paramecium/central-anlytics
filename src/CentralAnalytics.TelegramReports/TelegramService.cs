using Telegram.Bot;
using Telegram.Bot.Exceptions;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;

namespace CentralAnalytics.TelegramReports;

public class TelegramService
{
    private Func<ITelegramBotClient, Update, CancellationToken, Task> HandleAction { get; }
    private long RecipientId { get; }
    public TelegramBotClient BotClient { get; set; }

    public TelegramService(
        TelegramReportsOptions options,
        Func<ITelegramBotClient, Update, CancellationToken, Task>? handler = null)
    {
        HandleAction = handler ?? ((_,_,_) => Task.CompletedTask);
        BotClient = new TelegramBotClient(options.Key);
        RecipientId = options.RecipientTelegramId;

        BotClient.StartReceiving(
            new DefaultUpdateHandler(
                HandleUpdateAsync, 
                HandleErrorAsync), 
            null);
    }

    private Task HandleErrorAsync(
        ITelegramBotClient botClient, 
        Exception exception,
        CancellationToken cancellationToken)
    {
        if (exception is ApiRequestException apiRequestException)
        {
            Console.WriteLine("BOT ERROR: " + apiRequestException);
        }

        return Task.CompletedTask;
    }

    
    private Task HandleUpdateAsync(
        ITelegramBotClient botClient,
        Update update,
        CancellationToken ct)
    {
        return HandleAction(botClient, update, ct);
    }

    public async Task SendMessage(
        string message,
        CancellationToken ct)
    {
        await BotClient
            .SendTextMessageAsync(
                RecipientId,
                message,
                ParseMode.Markdown, 
                cancellationToken: ct);
    }
}