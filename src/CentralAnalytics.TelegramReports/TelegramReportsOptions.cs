namespace CentralAnalytics.TelegramReports;

public class TelegramReportsOptions
{
    public string Key { get; set; }
    public long RecipientTelegramId { get; set; }
}