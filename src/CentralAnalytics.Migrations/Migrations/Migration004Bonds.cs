﻿using FluentMigrator;

namespace CentralAnalytics.Migrations.Migrations;

[Migration(202304110000, "Bonds table")]
public class Migration004Bonds : AutoReversingMigration
{
    public override void Up()
    {
        Create
            .Table("bonds")
            .InSchema("instrument")
            .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
            .WithColumn("ticker").AsString().NotNullable()
            .WithColumn("figi").AsString().NotNullable()
            .WithColumn("isin").AsString().NotNullable()
            .WithColumn("currency").AsString().NotNullable()
            .WithColumn("maturity_date").AsDateTime().NotNullable()
            .WithColumn("placement_date").AsDateTime().NotNullable()
            .WithColumn("country_of_risk_name").AsString().NotNullable()
            .WithColumn("sector").AsString().NotNullable()
            .WithColumn("is_active").AsBoolean().NotNullable()
            .WithColumn("payload").AsCustom("jsonb").Nullable();
    }
}