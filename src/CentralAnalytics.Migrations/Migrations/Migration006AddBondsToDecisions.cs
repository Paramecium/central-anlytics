﻿using FluentMigrator;

namespace CentralAnalytics.Migrations.Migrations;

[Migration(202304221800, "Add Bonds To Decisions")]
public class Migration006AddBondsToDecisions: AutoReversingMigration
{
    public override void Up()
    {
        Create
            .Table("decision_bonds")
            .InSchema("decision")
            .WithColumn("decision_id").AsInt32()
            .WithColumn("bond_id").AsInt32();
        
        Create
            .ForeignKey()
            .FromTable("decision_bonds").InSchema("decision").ForeignColumn("decision_id")
            .ToTable("decision").InSchema("decision").PrimaryColumn("id");
        
        Create
            .ForeignKey()
            .FromTable("decision_bonds").InSchema("decision").ForeignColumn("bond_id")
            .ToTable("bonds").InSchema("instrument").PrimaryColumn("id");
    }
}