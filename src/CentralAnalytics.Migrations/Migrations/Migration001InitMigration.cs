using FluentMigrator;

namespace CentralAnalytics.Migrations.Migrations;

[Migration(202211200000, "Database initialization")]
public class Migration001InitMigration : AutoReversingMigration
{
    public override void Up()
    {
        Create.Schema("country");
        Create
            .Table("country")
            .InSchema("country")
            .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
            .WithColumn("name").AsString().NotNullable();
        
        Create
            .Table("central_bank")
            .InSchema("country")
            .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
            .WithColumn("name").AsString().NotNullable()
            .WithColumn("site").AsString().Nullable()
            .WithColumn("country_id").AsInt32().NotNullable();

        Create
            .ForeignKey()
            .FromTable("central_bank").InSchema("country").ForeignColumn("country_id")
            .ToTable("country").InSchema("country").PrimaryColumn("id");

        Create.Schema("company");
        Create
            .Table("company")
            .InSchema("company")
            .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
            .WithColumn("name").AsString().NotNullable()
            .WithColumn("country_id").AsInt32().NotNullable();
        
        Create
            .ForeignKey()
            .FromTable("company").InSchema("company").ForeignColumn("country_id")
            .ToTable("country").InSchema("country").PrimaryColumn("id");
        
        Create
            .Table("exchange")
            .InSchema("instrument")
            .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
            .WithColumn("country_id").AsInt32().Nullable()
            .WithColumn("name").AsString().NotNullable();
        
        Create
            .ForeignKey()
            .FromTable("exchange").InSchema("instrument").ForeignColumn("country_id")
            .ToTable("country").InSchema("country").PrimaryColumn("id");

        Create
            .Table("dividend")
            .InSchema("instrument")
            .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
            .WithColumn("instrument_id").AsInt32().NotNullable()
            .WithColumn("amount").AsDecimal().NotNullable()
            .WithColumn("currency").AsString().NotNullable()
            .WithColumn("register_closing_date").AsDate().Nullable();

        Create.PrimaryKey("PK_instrument_id").OnTable("instrument").WithSchema("instrument").Column("id");
        
        Create
            .ForeignKey()
            .FromTable("dividend").InSchema("instrument").ForeignColumn("instrument_id")
            .ToTable("instrument").InSchema("instrument").PrimaryColumn("id");
    }
}
