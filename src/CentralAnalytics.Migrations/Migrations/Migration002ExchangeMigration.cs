﻿using FluentMigrator;

namespace CentralAnalytics.Migrations.Migrations;

[Migration(202211220000, "Exchange ")]
public class Migration002ExchangeMigration : AutoReversingMigration
{
    public override void Up()
    {
        Alter.Table("exchange").InSchema("instrument")
            .AddColumn("acronym").AsString().Nullable()
            .AddColumn("type").AsInt16().Nullable();
        
        Create
            .Table("broker")
            .InSchema("instrument")
            .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
            .WithColumn("name").AsString().NotNullable()
            .WithColumn("site").AsString().Nullable()
            .WithColumn("country_id").AsInt32().NotNullable();

        Create
            .Table("broker_exchanges")
            .InSchema("instrument")
            .WithColumn("broker_id").AsInt32().NotNullable()
            .WithColumn("exchange_id").AsInt32().NotNullable();
    }
}