using FluentMigrator;

namespace CentralAnalytics.Migrations.Migrations;

[Migration(202301050000, "Instrument tables")]
public class Migration003InstrumentTables: AutoReversingMigration
{
    public override void Up()
    {
        Create
            .ForeignKey()
            .FromTable("broker_exchanges").InSchema("instrument").ForeignColumn("broker_id")
            .ToTable("broker").InSchema("instrument").PrimaryColumn("id");
        
        Create
            .ForeignKey()
            .FromTable("broker_exchanges").InSchema("instrument").ForeignColumn("exchange_id")
            .ToTable("exchange").InSchema("instrument").PrimaryColumn("id");

        Create
            .Table("cryptocurrency")
            .InSchema("instrument")
            .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
            .WithColumn("name").AsString().NotNullable()
            .WithColumn("ticker").AsString().NotNullable()
            .WithColumn("is_active").AsBoolean().NotNullable();
    }
}