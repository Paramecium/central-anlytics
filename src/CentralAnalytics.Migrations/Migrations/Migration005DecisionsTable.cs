﻿using FluentMigrator;

namespace CentralAnalytics.Migrations.Migrations;

[Migration(202304220000, "Decision table")]
public class Migration005DecisionsTable: AutoReversingMigration
{
    public override void Up()
    {
        Create.Schema("decision");
        
        Create
            .Table("decision")
            .InSchema("decision")
            .WithColumn("id").AsInt32().NotNullable().PrimaryKey().Identity()
            .WithColumn("text").AsString().NotNullable()
            .WithColumn("result").AsInt32().NotNullable();

        Create
            .Table("decision_instruments")
            .InSchema("decision")
            .WithColumn("decision_id").AsInt32()
            .WithColumn("instrument_id").AsInt32();
        
        Create
            .ForeignKey()
            .FromTable("decision_instruments").InSchema("decision").ForeignColumn("decision_id")
            .ToTable("decision").InSchema("decision").PrimaryColumn("id");
        
        Create
            .ForeignKey()
            .FromTable("decision_instruments").InSchema("decision").ForeignColumn("instrument_id")
            .ToTable("instrument").InSchema("instrument").PrimaryColumn("id");
    }
}