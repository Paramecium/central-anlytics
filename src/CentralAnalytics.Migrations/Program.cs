﻿using CentralAnalytics.Migrations.Infrastructure;

namespace CentralAnalytics.Migrations;

public static class Program
{
    private static void Main(
        string[] args)
    {
        MigrationHelper.RunInteractive(args);
    }
}
