namespace CentralAnalytics.Migrations.Infrastructure;

public record EnvironmentConfiguration(
    string ConnectionString);

public record AppSettingsConfiguration(
    Dictionary<string, EnvironmentConfiguration> Environments);
