using System.Globalization;
using System.Text.Json;
using FluentMigrator.Runner;
using Microsoft.Extensions.DependencyInjection;

namespace CentralAnalytics.Migrations.Infrastructure;

public static class MigrationHelper
{
    public static bool RunInteractive(
        string[] args)
    {
        var (env, configurationString) = GetConnectionString(args);
        if (string.IsNullOrEmpty(configurationString))
        {
            Console.WriteLine("Error while retrieving ConnectionString.");
            return false;
        }
        var serviceProvider = CreateServices(configurationString);

        var runner = serviceProvider.GetRequiredService<IMigrationRunner>();
        using var scope = serviceProvider.CreateScope();
        runner.ListMigrations();
        while (true)
        {
            try
            {
                Console.Write(env + " > ");
                var str = Console.ReadLine();
                if (!string.IsNullOrWhiteSpace(str))
                {
                    var fields = str
                        .ToLowerInvariant()
                        .Split(" ", StringSplitOptions.RemoveEmptyEntries);

                    switch (fields[0])
                    {
                        case "close":
                        case "exit":
                        case "quit":
                            return false;
                        case "down":
                            var downMigrationNumber = ReadLong(fields, 1);
                            runner.MigrateDown(downMigrationNumber.GetValueOrDefault());
                            continue;
                        case "env":
                            Console.WriteLine();
                            Console.WriteLine();
                            return true;
                        case "help":
                            Console.WriteLine("    up   [version]    Up migration");
                            Console.WriteLine("    down [version]    Down migration");
                            Console.WriteLine("    list              Shows migrations list");
                            Console.WriteLine("    env               Changes current environment");
                            Console.WriteLine("    exit              Closes the application");
                            continue;
                        case "list":
                            runner.ListMigrations();
                            continue;
                        case "up":
                            var upMigrationNumber = ReadLong(fields, 1);
                            if (upMigrationNumber.HasValue)
                            {
                                runner.MigrateUp(upMigrationNumber.Value);
                                continue;
                            }

                            runner.MigrateUp();
                            continue;
                        default:
                            Console.WriteLine("Unknown command.");
                            continue;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.GetType().Name + " " + ex.Message);
            }
        }
    }

    /// <summary>
    /// Чтение файла настроек и получение environment + configurationString
    /// </summary>
    private static (string env, string configurationString) GetConnectionString(string[] args)
    {
        string configurationString;
        try
        {
            using var sr = new StreamReader("../../../appsettings.json");
            configurationString = sr.ReadToEnd();
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error while reading 'appsettings.json': {ex.Message}");
            return (string.Empty, string.Empty);
        }

        var config =
            JsonSerializer.Deserialize<AppSettingsConfiguration>(configurationString);

        if (config is null || config.Environments.Count is 0)
        {
            Console.WriteLine("Error while parsing 'appsettings.json': zero ConnectionString-s.");
            return (string.Empty, string.Empty);
        }

        if (args.Length > 0)
        {
            if (config.Environments.ContainsKey(args[0]))
            {
                Console.WriteLine($"Command line args: found {args[0]} environment.");
                return (args[0], config.Environments[args[0]].ConnectionString);
            }
        }

        while (true)
        {
            Console.WriteLine($"Choose environment: {string.Join(", ", config.Environments.Keys)}");
            var input = Console.ReadLine() ?? string.Empty;
            if (config.Environments.ContainsKey(input))
            {
                return (input, config.Environments[input].ConnectionString);
            }
        }
    }

    /// <summary>
    /// Configure the dependency injection services
    /// </summary>
    private static IServiceProvider CreateServices(
        string connectionString)
    {
        return new ServiceCollection()
            // Add common FluentMigrator services
            .AddFluentMigratorCore()
            .ConfigureRunner(rb => rb
                // Add SQLite support to FluentMigrator
                .AddPostgres()
                // Set the connection string
                .WithGlobalConnectionString(connectionString)
                // Define the assembly containing the migrations
                .ScanIn(typeof(Program).Assembly).For.Migrations())
            // Enable logging to console in the FluentMigrator way
            .AddLogging(lb => lb.AddFluentMigratorConsole())
            // Build the service provider
            .BuildServiceProvider(false);
    }

    /// <summary>
    /// Чтение long с указанного индекса последовательности строк.
    /// </summary>
    private static long? ReadLong(
        IReadOnlyList<string> fields,
        int index) =>
        fields.Count < index + 1
            ? new long?()
            : long.Parse(fields[index], CultureInfo.InvariantCulture);
}
