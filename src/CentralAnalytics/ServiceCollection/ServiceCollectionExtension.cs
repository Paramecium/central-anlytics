﻿using System.Threading.Channels;
using CentralAnalytics.BLL.DefineInstrument;
using CentralAnalytics.BLL.Kafka.NewsAnalyzedEvent;
using CentralAnalytics.BLL.Options;
using CentralAnalytics.BLL.Rule;
using CentralAnalytics.BLL.Services;
using CentralAnalytics.BLL.WBLL;
using CentralAnalytics.DAL.Bonds;
using CentralAnalytics.DAL.CentralBank;
using CentralAnalytics.DAL.Decision;
using CentralAnalytics.DAL.Dividend;
using CentralAnalytics.DAL.Instrument;
using CentralAnalytics.DAL.WDAL;
using CentralAnalytics.DAL.WDAL.Gate;
using CentralAnalytics.DAL.WDAL.Tinkoff;
using CentralAnalytics.Shared.Infrastructure.Configuration;
using CentralAnalytics.Shared.Infrastructure.ConnectionFactory;
using CentralAnalytics.Shared.Infrastructure.Kafka.Consumer;
using CentralAnalytics.Shared.Infrastructure.Kafka.Deserializers;
using CentralAnalytics.Shared.Infrastructure.Kafka.KafkaEvent;
using CentralAnalytics.Shared.Infrastructure.Metrics;
using CentralAnalytics.Shared.Models;
using CentralAnalytics.TelegramReports;

namespace CentralAnalytics.ServiceCollection;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection RegisterOptions(this IServiceCollection services,
        IConfiguration configuration)
    {
        services.Configure<DefineInstrumentOptions>(configuration.GetSection(nameof(DefineInstrumentOptions)));
        services.Configure<TextSimilarityOptions>(configuration.GetSection(nameof(TextSimilarityOptions)));
        services.Configure<FeedReaderOptions>(configuration.GetSection(nameof(FeedReaderOptions)));
        services.Configure<GateOptions>(configuration.GetSection(nameof(GateOptions)));
        services.Configure<AnalyzedArticleConsumerSettings>(configuration.GetSection(nameof(AnalyzedArticleConsumerSettings)));
        services.Configure<TelegramReportsOptions>(configuration.GetSection(nameof(TelegramReportsOptions)));
        services.Configure<PgConnectionOptions>(configuration.GetSection(nameof(PgConnectionOptions)));
        return services;
    }

    public static IServiceCollection RegisterRules(this IServiceCollection services)
    {
        services.AddSingleton<IKeyRateDefineBondsService, KeyRateDefineBondsService>();
        services.AddSingleton<KeyRateRule>();
        services.AddSingleton<IDefineStocksService, DefineStocksService>();
        services.AddSingleton<CorporateReportRule>();
        services.AddSingleton<BuyBackRule>();
        services.AddSingleton<ExtraEmissionRule>();
        services.AddSingleton<DelistingRule>();
        services.AddSingleton<CorporateDividendsRule>();
        return services;
    }
    
    public static IServiceCollection RegisterServices(this IServiceCollection services)
    {
        services.AddSingleton<ITextSimilarityService, TextSimilarityService>();
        services.AddSingleton<IFeedReaderService, FeedReaderService>();


        return services;
    }

    public static IServiceCollection RegisterPgDatabase(
        this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddSingleton<IPgConnectionProvider, PgConnectionProvider>();

        return services;
    }

    /// <summary>
    /// Регистрация репозиториев уровня data-access.
    /// </summary>
    public static IServiceCollection RegisterRepositories(
        this IServiceCollection services)
    {
        services.AddSingleton<IInstrumentRepository, InstrumentRepository>();
        services.AddSingleton<IBondsRepository, BondsRepository>();
        services.AddSingleton<ICentralBankRepository, CentralBankRepository>();
        services.AddSingleton<IDecisionRepository, DecisionRepository>();
        services.AddSingleton<IDividendRepository, DividendRepository>();

        return services;
    }
    
    public static IServiceCollection RegisterClients(
        this IServiceCollection services)
    {
        return services.AddSingleton<TinkoffClient.TinkoffClient>();
    }
    
    public static IServiceCollection RegisterHostedServices(
        this IServiceCollection services,
        IConfiguration configuration)
    {
        services.AddSingleton<TinkoffMarketStreamHostedService>();
        services.AddSingleton<GateMarketStreamHostedService>();
        services.AddSingleton<IMarketListenerService>(sp => sp.GetRequiredService<TinkoffMarketStreamHostedService>());
        services.AddSingleton<IMarketListenerService>(sp => sp.GetRequiredService<GateMarketStreamHostedService>());
        services.AddSingleton<IMarketServiceProvider, MarketListenerServiceProvider>();
        services.AddHostedService(sp => sp.GetRequiredService<TinkoffMarketStreamHostedService>());
        services.AddHostedService(sp => sp.GetRequiredService<GateMarketStreamHostedService>());

        services.AddHostedService<MarketStreamHostedService>();
        return services;
    }
    
    public static IServiceCollection RegisterMetrics(
        this IServiceCollection services)
    {
        services.AddSingleton<IInstrumentMetrics, InstrumentMetrics>();
        services.AddSingleton<IProcessedMessagesMetrics, ProcessedMessagesMetrics>();
        return services;
    }
    
    public static IServiceCollection RegisterChannels(
        this IServiceCollection services)
    {
        // Канал для получения инфы в бизнес-слой
        services.AddSingleton(_ => Channel.CreateUnbounded<TinkoffMarketStreamResponse>());

        return services;
    }

    /// <summary>
    /// Регистрация сервисов уровня бизнес-логики.
    /// </summary>
    public static IServiceCollection RegisterBll(
        this IServiceCollection services,
        IConfiguration configuration,
        IWebHostEnvironment environment)
    {
        services.RegisterKafka(configuration, environment);
        return services;
    }

    public static void RegisterKafka(
        this IServiceCollection services,
        IConfiguration configuration,
        IWebHostEnvironment environment)
    {
        // ------------- Deserializers -------------
        services.AddSingleton<IKafkaDeserializer, JsonKafkaDeserializer>();

        // --------------- Producers ---------------
        // TODO добавить, когда понадобится

        // --------------- Consumers ---------------
        // TODO Возможно, потом понадобится
        // var environmentName = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
        // var ignoreKafkaTopic = environmentName is "Development" or "Testing";
        // if (ignoreKafkaTopic)
        // {
        //     return;
        // }

        services.AddSingleton<IKafkaEventHandler<AnalyzedArticle>, AnalyzedArticleHandler>();
        services.AddSingleton<AnalyzedArticleHandler>();
        // Кафка обработанных новостей
        services.AddKafkaConsumer<AnalyzedArticleConsumerSettings, NewsAnalyzedEventTypeParser>(
            builder =>
            {
                builder
                    .WithHandler<AnalyzedArticle, AnalyzedArticleHandler, IKafkaDeserializer>();
            });
    }

    private static void AddKafkaConsumer<TSettings, TEventTypeParser>(
        this IServiceCollection serviceCollection,
        Action<KafkaConsumerBuilder> configureHandlers)
        where TSettings : class, IKafkaEventsConsumerSettings
        where TEventTypeParser : class, IKafkaEventTypeParser
    {
        var builder = new KafkaConsumerBuilder();
        configureHandlers(builder);

        serviceCollection.AddHostedService<KafkaConsumerService<TSettings>>();
        serviceCollection.AddSingleton<IKafkaConsumer<TSettings>>(sp =>
        {
            var resolver = ActivatorUtilities.CreateInstance<TEventTypeParser>(sp);
            return ActivatorUtilities.CreateInstance<KafkaConsumer<TSettings>>(sp, resolver, builder.Descriptions);
        });
    }
}