﻿using CentralAnalytics.BLL.Kafka.NewsAnalyzedEvent;
using CentralAnalytics.Shared.Models;
using Microsoft.AspNetCore.Mvc;

namespace CentralAnalytics.Controllers;

[Route("[Controller]")]
public class EventController : ControllerBase
{
    private readonly AnalyzedArticleHandler _eventHandler;
    private readonly ILogger<EventController> _logger;

    public EventController(AnalyzedArticleHandler eventHandler,
        ILogger<EventController> logger)
    {
        _logger = logger;
        _eventHandler = eventHandler;
    }

    [Route("handle")]
    [HttpPost]
    public async Task<IActionResult> Handle([FromBody] AnalyzedArticle message, CancellationToken ct)
    {
        try
        {
            if (message == null) return BadRequest("Invalid message");
            var result = await _eventHandler.AnalyzeMessage(message, ct);
            return Ok(new { Result = result.AnalyzeResult.ToString(), MessageToUser = result.Message });
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Exception occurs while processing event");
            return BadRequest("Invalid message");
        }
    }
}