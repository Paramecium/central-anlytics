﻿using CentralAnalytics.DAL.Bonds;
using CentralAnalytics.DAL.Instrument;
using Microsoft.AspNetCore.Mvc;
using Tinkoff.InvestApi.V1;
using Bond = CentralAnalytics.DAL.Bonds.Bond;

namespace CentralAnalytics.Controllers;

[Route("[Controller]")]
public class InstrumentController: ControllerBase
{
    private readonly IInstrumentRepository _instrumentRepository;
    private readonly IBondsRepository _bondsRepository;
    private readonly ILogger<InstrumentController> _logger;
    private readonly TinkoffClient.TinkoffClient _tinkoffClient;

    public InstrumentController(IInstrumentRepository instrumentRepository, 
        IBondsRepository bondsRepository,
        TinkoffClient.TinkoffClient tinkoffClient,
        ILogger<InstrumentController> logger)
    {
        _instrumentRepository = instrumentRepository;
        _bondsRepository = bondsRepository;
        _logger = logger;
        _tinkoffClient = tinkoffClient;
    }

    [Route("Reinitialize")]
    [HttpPost]
    public async Task<IActionResult> Reinitialize(CancellationToken ct)
    {
        try
        {
            await _instrumentRepository.Reinitialize(ct);
            return Ok("Reinitialization completed");
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Exception occurs while reinitializing instrument repository");
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }
    
    [Route("ActivateByFigis")]
    [HttpPost]
    public async Task<IActionResult> ActivateInstrumentsByFigis([FromBody] IReadOnlyList<string> figis, CancellationToken ct)
    {
        try
        {
            await _instrumentRepository.ActivateInstrumentsByFigis(figis , ct);
            return Ok("Instruments activated successfully");
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Exception occurs during instruments activation");
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }
    
    [Route("LoadBonds")]
    [HttpPost]
    public async Task<IActionResult> LoadBonds(CancellationToken ct)
    {
        try
        {
            var instruments = _tinkoffClient.GetBonds(new InstrumentsRequest {InstrumentStatus = InstrumentStatus.Base});
            await _bondsRepository.InsertBonds(instruments.Instruments.Select(Bond.FromTinkoffBond), ct);
            return Ok("Bonds loaded successfully");
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Exception occurs during bonds loading");
            return StatusCode(StatusCodes.Status500InternalServerError, e.Message);
        }
    }
}