﻿using System.Text.Json;
using CentralAnalytics.BLL.Services;
using CentralAnalytics.DAL.Bonds;
using CentralAnalytics.DAL.Instrument;
using Microsoft.AspNetCore.Mvc;
using MoreLinq;

namespace CentralAnalytics.Controllers;

[Route("[Controller]")]
public class EmbeddingController : ControllerBase
{
    private readonly IBondsRepository _bondsRepository;
    private readonly ITextSimilarityService _textSimilarityService;
    private readonly IInstrumentRepository _instrumentRepository;
    
    public EmbeddingController(IBondsRepository bondsRepository, ITextSimilarityService textSimilarityService, IInstrumentRepository instrumentRepository)
    {
        _bondsRepository = bondsRepository;
        _textSimilarityService = textSimilarityService;
        _instrumentRepository = instrumentRepository;
    }
    
    [Route("update_bond_embeddings")]
    [HttpPost]
    public async Task<IActionResult> UpdateBonds(CancellationToken ct)
    {
        var bonds = await _bondsRepository.GetAllBondsFromPostgres(ct);
        foreach (var batch in bonds.Batch(10))
        {
            var embeddings = await _textSimilarityService.GetEmbeddingsAsync(batch.Select(x => x.CountryOfRiskName), ct);
            var bondsWithEmbeddings = batch.Zip(embeddings).Select(x => x.First with {Embedding = JsonSerializer.Serialize(x.Second)}).ToList();
            await _bondsRepository.UpdateEmbeddings(bondsWithEmbeddings, ct);
        }

        return Ok("Embeddings updated successfully");
    }
    
    [Route("update_instrument_embeddings")]
    [HttpPost]
    public async Task<IActionResult> UpdateInstruments(CancellationToken ct)
    {
        var instruments = await _instrumentRepository.GetAllActiveInstrumentsFromPostgres(ct);
        foreach (var batch in instruments.Batch(10))
        {
            var embeddings = await _textSimilarityService.GetEmbeddingsAsync(batch.Select(x => x.Name), ct);
            var instrumentsWithEmbeddings = batch.Zip(embeddings).Select(x => x.First with {Embedding = JsonSerializer.Serialize(x.Second)}).ToList();
            await _instrumentRepository.UpdateEmbeddings(instrumentsWithEmbeddings, ct);
        }

        return Ok("Embeddings updated successfully");
    }
}