using CentralAnalytics.ServiceCollection;
using CentralAnalytics.TelegramReports;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using Prometheus;

namespace CentralAnalytics;

public class Startup
{
    private readonly IConfiguration _configuration;
    private readonly IWebHostEnvironment _environment;

    public Startup(
        IConfiguration configuration,
        IWebHostEnvironment environment)
    {
        _configuration = configuration;
        _environment = environment;
    }

    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(
        IServiceCollection services)
    {
        services.AddControllers();
        services.AddEndpointsApiExplorer();
        services.AddSwaggerGen(c =>
        {
            c.SwaggerDoc("v1", new OpenApiInfo { Title = "CentralAnalytics", Version = "v1" });
        });
        services.AddHttpClient();

        services.AddMemoryCache();
        services
            .RegisterOptions(_configuration)
            .RegisterRules()
            .RegisterChannels()
            .RegisterClients()
            .RegisterHostedServices(_configuration)
            .RegisterMetrics()
            .RegisterServices()
            .RegisterPgDatabase(_configuration)
            .RegisterRepositories()
            .RegisterBll(_configuration, _environment);
        
        
        services.AddSingleton<TelegramService>(x=> 
            new TelegramService(
                x.GetRequiredService<IOptions<TelegramReportsOptions>>().Value,
                (client, updateProps, ct) =>
                {
                    return Task.CompletedTask;
                }));
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        app.UseDeveloperExceptionPage();
        app.UseSwagger();
        app.UseMetricServer();
        
        if (env.IsProduction() is false)
        {
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "CentralAnalytics v1");

            });
        }
        else
        {
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "CentralAnalytics v1 PROD");
            });
        }
        app.UseRouting();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
        });
    }
}
