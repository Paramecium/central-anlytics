using Serilog;
using Serilog.Events;
using Serilog.Exceptions;
using Serilog.Sinks.TelegramBot;

namespace CentralAnalytics;

public class Program
{
    public static void Main(
        string[] args)
    {
        CreateHostBuilder(args).Build().Run();
    }

    public static IHostBuilder CreateHostBuilder(
        string[] args) =>
        Host.CreateDefaultBuilder(args)
            .ConfigureWebHostDefaults(webBuilder =>
            {
                webBuilder.UseStartup<Startup>();
            })
#if !DEBUG
            .UseSerilog(
                new LoggerConfiguration()
                    .MinimumLevel.Information()
                    .Enrich.WithExceptionDetails()
                    .WriteTo.TelegramBot(
                        "5709613012:AAEYZHmd_5xLzk1TJIw-AV0u59C7l6f_H6U",
                        "-1001404735141",
                        $"CentralAnalytics {Environment.MachineName} - {Environment.ProcessId}" ,
                        restrictedToMinimumLevel:LogEventLevel.Warning)
                    .WriteTo.Logger(
                        config =>
                            config
                                .Enrich.WithExceptionDetails()
                                .WriteTo.Console())
                    .CreateLogger());
#else
            ;
#endif
}
