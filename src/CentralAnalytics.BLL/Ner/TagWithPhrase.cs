﻿namespace CentralAnalytics.BLL.Ner;

public record TagWithPhrase(NamedEntity Tag, string Phrase, int StartIndex);