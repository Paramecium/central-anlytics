﻿namespace CentralAnalytics.BLL.Ner;

public enum NamedEntity
{
    None,
    Organisation,
    GPE, // Countries, cities, states
    Product,
    Date,
    Time,
    Percent,
    Money,
    Quantity,
    Cardinal
}