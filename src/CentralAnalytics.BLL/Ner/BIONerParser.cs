﻿using System.Text;

namespace CentralAnalytics.BLL.Ner;

public static class BIONerParser
{
    private static Dictionary<string, NamedEntity> TagsВуNotations = new ()
    {
        ["ORG"] = NamedEntity.Organisation,
        ["GPE"] = NamedEntity.GPE,
        ["PRODUCT"] = NamedEntity.Product,
        ["DATE"] = NamedEntity.Date, 
        ["TIME"] = NamedEntity.Time,
        ["QUANTITY"] = NamedEntity.Quantity,
        ["PERCENT"] = NamedEntity.Percent,
        ["MONEY"] = NamedEntity.Money,
        ["CARDINAL"] = NamedEntity.Cardinal
    };

    private const string BlankTag = "O";
        
    public static List<TagWithPhrase> ParseNer(IReadOnlyList<string> words, IReadOnlyList<string> tags)
    {
        var response = new List<TagWithPhrase>();
        var currentPhraseBuilder = new StringBuilder();
        var currentTag = NamedEntity.None;
        var startIndex = 0;
        var tagsWithWords = tags.Zip(words, (tag, word) => new { Tag = tag, Word = word });
        
        foreach (var (item, index) in tagsWithWords.Select((tagWithWord, i) => (tagWithWord, i)))
        {
            if (item.Tag.Equals(BlankTag))
            {
                TryAddToResponse();
                continue;
            }

            if (item.Tag.StartsWith("B-"))
            {
                TryAddToResponse();
                if (TagsВуNotations.TryGetValue(item.Tag[2..], out currentTag))
                {
                    startIndex = index;
                    currentPhraseBuilder.Append(item.Word);
                }
            }
            else
            {
                if (TagsВуNotations.TryGetValue(item.Tag[2..], out currentTag))
                {
                    currentPhraseBuilder.Append(' ');
                    currentPhraseBuilder.Append(item.Word);
                }
            }
        }
        
        TryAddToResponse();

        return response;


        void TryAddToResponse()
        {
            if (currentPhraseBuilder.Length > 0)
            {
                response.Add(new TagWithPhrase(currentTag, currentPhraseBuilder.ToString(), startIndex));
                currentPhraseBuilder.Clear();
            }
        }
    }
}
