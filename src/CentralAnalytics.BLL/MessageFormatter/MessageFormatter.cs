﻿using System.Text;
using CentralAnalytics.BLL.Models;
using CentralAnalytics.DAL.Instrument;
using Newtonsoft.Json;

namespace CentralAnalytics.BLL.MessageFormatter;

public static class MessageFormatter
{
    public static string GetFormattedText(AnalyzedArticleProcessingContext context, string SourceTitle)
    {
        var sb = new StringBuilder();
        sb.Append($"Получена новость из источника {SourceTitle}").AppendLine();
        sb.Append($"Текст новости: {context.AnalyzedArticle.Summary}").AppendLine();
        
        switch (context.Result)
        {
            case AnalyzeResult.Up:
                sb.Append("Новость может оказать положительное влияние на цены активов:").AppendLine();
                break;
            case AnalyzeResult.Down:
                sb.Append("Новость может оказать негативное влияние на цены активов:").AppendLine();
                break;
            default:
                break;
        }
        
        foreach (var bond in context.PossibleBonds.Take(15))
        {
            sb.Append($"[{bond.Name}](https://www.tinkoff.ru/invest/bonds/{bond.Ticker}/)").AppendLine();
        }
        
        foreach (var instrument in context.PossibleInstruments)
        {
            sb.Append($"[{instrument.Name}](https://www.tinkoff.ru/invest/stocks/{instrument.Ticker}/)").AppendLine();
        }

        return sb.ToString();
    }
}