﻿using System.Globalization;
using System.Text.RegularExpressions;
using CentralAnalytics.BLL.DefineInstrument;
using CentralAnalytics.BLL.Models;
using CentralAnalytics.BLL.Ner;
using CentralAnalytics.DAL.Dividend;
using CentralAnalytics.Shared.Models;

namespace CentralAnalytics.BLL.Rule;

public class CorporateDividendsRule: IRule
{
    private readonly IDefineStocksService _defineStocksService;
    private readonly IDividendRepository _dividendRepository;

    public CorporateDividendsRule(IDefineStocksService defineStocksService, IDividendRepository dividendRepository)
    {
        _defineStocksService = defineStocksService;
        _dividendRepository = dividendRepository;
    }
    
    public async Task ApplyAsync(AnalyzedArticleProcessingContext context, CancellationToken token)
    {
        if (context.AnalyzedArticle?.SecondType is not (SecondLevelSentimentType.CorporateDividend))
        {
            return;
        }
        
        context.PossibleInstruments = (await _defineStocksService.DefineStocksFromArticle(context, token)).ToList();
        if (context.PossibleInstruments.Any())
        {
            var moneyEntities = context.RecognizedEntities.Where(x => x.Tag == NamedEntity.Money).ToList();
            var divAmountString = "";
            if (!moneyEntities.Any() || moneyEntities.Count() > 2) return;
            if (moneyEntities.Count() == 2)
            {
                divAmountString =
                    Regex.Replace(moneyEntities.ElementAt(0).Phrase, "[^0-9]", "") + "." +
                    Regex.Replace(moneyEntities.ElementAt(1).Phrase, "[^0-9]", "");
            }
            else
            {
                divAmountString = Regex.Replace(moneyEntities.ElementAt(0).Phrase, "[^0-9.,]", "").Replace(",", ".");
            }

            if (!double.TryParse(divAmountString, NumberStyles.Any, CultureInfo.InvariantCulture, out var divAmount)) return;
            
            var storedDividends = await _dividendRepository.GetDividendsByInstrumentId(context.PossibleInstruments.First().Id, token);
            var nearestDividend = storedDividends.Where(x => x.RegisterClosingDate != null && x.RegisterClosingDate > DateTime.Now).MinBy(x => x.RegisterClosingDate);
            if (nearestDividend != null)
            {
                context.Result = nearestDividend.Amount < divAmount ? AnalyzeResult.Up : AnalyzeResult.Down;
            }
            
        }
    }
}