﻿using CentralAnalytics.BLL.DefineInstrument;
using CentralAnalytics.BLL.Models;
using CentralAnalytics.Shared.Models;

namespace CentralAnalytics.BLL.Rule;

public class CorporateReportRule : IRule
{
    private readonly IDefineStocksService _defineStocksService;

    public CorporateReportRule(IDefineStocksService defineStocksService)
    {
        _defineStocksService = defineStocksService;
    }
    
    public async Task ApplyAsync(AnalyzedArticleProcessingContext context, CancellationToken token)
    {
        if (context.AnalyzedArticle?.SecondType is not (SecondLevelSentimentType.CorporateReport))
        {
            return;
        }

        if (context.AnalyzedArticle.Sentiment == "positive" || context.AnalyzedArticle.Sentiment == "negative")
        {
            context.PossibleInstruments = (await _defineStocksService.DefineStocksFromArticle(context, token)).ToList();
            if (context.PossibleInstruments.Any())
            {
                context.Result = context.AnalyzedArticle.Sentiment == "positive" ? AnalyzeResult.Up : AnalyzeResult.Down;
            }
        }
    }
}