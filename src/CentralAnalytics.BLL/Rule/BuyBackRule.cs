﻿using CentralAnalytics.BLL.DefineInstrument;
using CentralAnalytics.BLL.Models;
using CentralAnalytics.Shared.Models;

namespace CentralAnalytics.BLL.Rule;

public class BuyBackRule: IRule
{
    private readonly IDefineStocksService _defineStocksService;

    public BuyBackRule(IDefineStocksService defineStocksService)
    {
        _defineStocksService = defineStocksService;
    }
    
    public async Task ApplyAsync(AnalyzedArticleProcessingContext context, CancellationToken token)
    {
        if (context.AnalyzedArticle?.SecondType is not (SecondLevelSentimentType.CorporateBuyback))
        {
            return;
        }
        
        context.PossibleInstruments = (await _defineStocksService.DefineStocksFromArticle(context, token)).ToList();
        if (context.PossibleInstruments.Any())
        {
            context.Result = AnalyzeResult.Up;
        }
    }
}