﻿using CentralAnalytics.BLL.DefineInstrument;
using CentralAnalytics.BLL.Models;
using CentralAnalytics.BLL.Services;
using CentralAnalytics.Shared.Models;

namespace CentralAnalytics.BLL.Rule;

public class KeyRateRule : IRule
{
    private readonly List<ChangeWithStatement> _rateChangeSentence = new()
    {
        new ChangeWithStatement("Банк не изменил ставку", KeyRateChange.None),
        new ChangeWithStatement(  "Банк повысил ставку", KeyRateChange.Increase),
        new ChangeWithStatement(  "Банк понизил ставку", KeyRateChange.Decrease)
    };
    
    private readonly ITextSimilarityService _similarityService;
    private readonly IKeyRateDefineBondsService _bondsService;

    public KeyRateRule(
        ITextSimilarityService similarityService,
        IKeyRateDefineBondsService bondsService)
    {
        _similarityService = similarityService;
        _bondsService = bondsService;
    }

    public async Task ApplyAsync(AnalyzedArticleProcessingContext context, CancellationToken token)
    {
        if (context.AnalyzedArticle.SecondType is not (SecondLevelSentimentType.EconomicalCBRate))
        {
            return;
        }

        var similarities = (await _similarityService.GetSimilarityAsync(_rateChangeSentence
                .Select(x => x.Statement)
                .Append(context.AnalyzedArticle.Summary), token)).ToList();

        // if (context.AnalyzedArticle.SecondType is SecondLevelType.CorporateReport && similarities.Max() < 0.5)
        // {
        //     return;
        // }
        var maxIndex = similarities.IndexOf(similarities.Max());
        var change = _rateChangeSentence[maxIndex].Change;
        
        if (change is KeyRateChange.Decrease or KeyRateChange.Increase)
        {
            context.PossibleBonds = (await _bondsService.DefineBondsFromArticle(context, token)).ToList();
            if (context.PossibleBonds.Any())
            {
                context.Result = change is KeyRateChange.Decrease ? AnalyzeResult.Up : AnalyzeResult.Down;
            }
        }
    }
    
    private enum KeyRateChange
    {
        None,
        Increase,
        Decrease
    }

    private record ChangeWithStatement(string Statement, KeyRateChange Change);
}