﻿using CentralAnalytics.BLL.Models;

namespace CentralAnalytics.BLL.Rule;

public interface IRule
{
    Task ApplyAsync(
        AnalyzedArticleProcessingContext context,
        CancellationToken token);

}