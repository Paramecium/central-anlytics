using System.Threading.Channels;
using CentralAnalytics.DAL.Instrument;
using CentralAnalytics.DAL.WDAL;
using CentralAnalytics.DAL.WDAL.Tinkoff;
using CentralAnalytics.Shared.Infrastructure.Metrics;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Tinkoff.InvestApi.V1;

namespace CentralAnalytics.BLL.WBLL;

public class MarketStreamHostedService: IHostedService, IDisposable
{
    private readonly IMarketServiceProvider _marketServiceProvider;
    private readonly IInstrumentRepository _instrumentRepository;
    private readonly ChannelReader<TinkoffMarketStreamResponse> _tinkoffMarketStreamResponseReader;
    private readonly ILogger<MarketStreamHostedService> _logger;
    private readonly IInstrumentMetrics _instrumentMetrics;

    public MarketStreamHostedService(
        IMarketServiceProvider marketServiceProvider,
        IInstrumentRepository instrumentRepository,
        Channel<TinkoffMarketStreamResponse> tinkoffMarketStreamResponseChannel,
        ILogger<MarketStreamHostedService> logger,
        IInstrumentMetrics instrumentMetrics)
    {
        _marketServiceProvider = marketServiceProvider;
        _instrumentRepository = instrumentRepository;
        _logger = logger;
        _instrumentMetrics = instrumentMetrics;
        _tinkoffMarketStreamResponseReader = tinkoffMarketStreamResponseChannel.Reader;
    }

    public async Task StartAsync(CancellationToken ct)
    {
        await _instrumentRepository.Reinitialize(ct);
        var activeInstruments =
            await _instrumentRepository.GetAllActiveInstrumentsFromPostgres(ct);

        var tinkoffListener = _marketServiceProvider.Get(MarketType.Tinkoff);
        await tinkoffListener.Subscribe(new SubscriptionRequest(
                activeInstruments.Select(x => x.Figi),
                MarketDataSubscriptionAction.Subscribe),
            ct);

        // var gateListener = _marketServiceProvider.Get(MarketType.Gate);
        // while (!gateListener.IsReady)
        // {
        //     await Task.Delay(500, ct);
        // }
        // await gateListener.Subscribe(new SubscriptionRequest(
        //         new[] { "BTC" },
        //         MarketDataSubscriptionAction.Subscribe),
        //     ct);

        ListenResponseStream(ct);
    }

    public Task StopAsync(CancellationToken cancellationToken)
    {
        throw new NotImplementedException();
    }

    public void Dispose()
    {
    }


    private Task ListenResponseStream(CancellationToken ct)
        => Task.Run(
            async () =>
            {
                await foreach (
                    var response
                    in _tinkoffMarketStreamResponseReader.ReadAllAsync(cancellationToken: ct))
                {
                    try
                    {
                        _instrumentMetrics.InstrumentLastPriceReceivedInc(
                            response.LastPrice.FinInstrument.Ticker);

                        _instrumentMetrics.InstrumentLastPriceLog(
                            response.LastPrice.FinInstrument.Ticker,
                            (double) response.LastPrice.Price);
                    }
                    catch (Exception e)
                    {
                        _logger.LogError(e, e.Message);
                    }
                }
            },
            ct);

    private static IEnumerable<LastPriceInstrument> ToTinkoffInstrument(
        IEnumerable<string> figiList)
        => figiList.Select(x => new LastPriceInstrument { Figi = x });

    private static decimal FromTinkoffQuotationToDecimal(Quotation quotation)
        => quotation.Units + new decimal(quotation.Nano) / 1_000_000_000M;
}


public record FinInstrumentPrice(
    DAL.Instrument.Instrument FinInstrument,
    decimal Price,
    DateTimeOffset Timestamp)
{
    public override string ToString()
    {
        return null;
    }
}
