﻿namespace CentralAnalytics.BLL.Options;

public class FeedReaderOptions
{
    public string Url { get; set; }
}