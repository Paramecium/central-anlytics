﻿using System.Text;
using CentralAnalytics.BLL.Options;
using Microsoft.Extensions.Options;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace CentralAnalytics.BLL.Services;

public interface ITextSimilarityService
{
    Task<IEnumerable<double>> GetSimilarityAsync(IEnumerable<string> sentences, CancellationToken ct);

    Task<IEnumerable<IEnumerable<double>>> GetEmbeddingsAsync(IEnumerable<string> sentences, CancellationToken ct);
    
    Task<IEnumerable<double>> GetSimilarityByEmbeddingsAsync(IEnumerable<string> embeddings, CancellationToken ct);
}

public class TextSimilarityService : ITextSimilarityService
{
    private const string GetSimilarityUrl = "/similarity";
    private const string GetEmbeddingsUrl = "/get-embeddings";
    private const string GetSimilarityByEmbeddingsUrl = "/similarity-by-embeddings";
    private readonly HttpClient _httpClient;

    public TextSimilarityService(IHttpClientFactory httpClientFactory, IOptions<TextSimilarityOptions> options)
    {
        _httpClient = httpClientFactory.CreateClient();
        _httpClient.BaseAddress = new Uri(options.Value.Url);
    }


    public async Task<IEnumerable<double>> GetSimilarityAsync(IEnumerable<string> sentences, CancellationToken ct)
    {
        var data = new StringContent(JsonSerializer.Serialize(new SimilarityRequest {Sentences = sentences}), Encoding.UTF8, "application/json");
        using var response = await _httpClient.PostAsync(GetSimilarityUrl, data, ct);
        var result = await JsonSerializer.DeserializeAsync<SimilarityResponse>(await response.Content.ReadAsStreamAsync(ct), cancellationToken: ct);
        return result?.Similarity.Skip(1) ?? Array.Empty<double>();
    }

    public async Task<IEnumerable<IEnumerable<double>>> GetEmbeddingsAsync(IEnumerable<string> sentences, CancellationToken ct)
    {
        var data = new StringContent(JsonSerializer.Serialize(new SimilarityRequest {Sentences = sentences}), Encoding.UTF8, "application/json");
        using var response = await _httpClient.PostAsync(GetEmbeddingsUrl, data, ct);
        var result = await JsonSerializer.DeserializeAsync<EmbeddingsResponse>(await response.Content.ReadAsStreamAsync(ct), cancellationToken: ct);
        return result.Embeddings;
    }

    public async Task<IEnumerable<double>> GetSimilarityByEmbeddingsAsync(IEnumerable<string> embeddings, CancellationToken ct)
    {
        var data = new StringContent(JsonSerializer.Serialize(new SimilarityByEmbeddingsRequest {Embeddings = embeddings.Select(x => JsonSerializer.Deserialize<IEnumerable<double>>(x) ?? Enumerable.Empty<double>())}), Encoding.UTF8, "application/json");
        using var response = await _httpClient.PostAsync(GetSimilarityByEmbeddingsUrl, data, ct);
        var result = JsonSerializer.Deserialize<SimilarityResponse>(await response.Content.ReadAsStringAsync(ct));
        return result?.Similarity.Skip(1) ?? Array.Empty<double>();
    }

    private class SimilarityRequest
    {
        [JsonPropertyName("Sentences")]
        public IEnumerable<string> Sentences { get; set; }
    }
    
    private class SimilarityResponse
    {
        [JsonPropertyName("Similarity")]
        public IEnumerable<double> Similarity { get; set; }
    }
    
    private class EmbeddingsResponse
    {
        [JsonPropertyName("Embeddings")]
        public IEnumerable<IEnumerable<double>> Embeddings { get; set; }
    }
    
    private class SimilarityByEmbeddingsRequest
    {
        [JsonPropertyName("Embeddings")]
        public IEnumerable<IEnumerable<double>> Embeddings { get; set; }
    }
}