﻿using System.Text;
using System.Text.Json.Serialization;
using CentralAnalytics.BLL.Options;
using Microsoft.Extensions.Options;
using System.Text.Json;

namespace CentralAnalytics.BLL.Services;

public interface IFeedReaderService
{
    Task<string> GetSourceNameById(long id, CancellationToken ct);
}

public class FeedReaderService : IFeedReaderService
{
    private const string GetSourceUrl = "/Source/getSourcesByIds";
    private readonly HttpClient _httpClient;
    
    public FeedReaderService(IHttpClientFactory httpClientFactory, IOptions<FeedReaderOptions> options)
    {
        _httpClient = httpClientFactory.CreateClient();
        _httpClient.BaseAddress = new Uri(options.Value.Url);
    }
    
    public async Task<string> GetSourceNameById(long id, CancellationToken ct)
    {
        var data = new StringContent(JsonSerializer.Serialize(new GetSourcesRequest {SourceIds = new []{id}}), Encoding.UTF8, "application/json");
        using var response = await _httpClient.PutAsync(GetSourceUrl, data, ct);
        var result = await JsonSerializer.DeserializeAsync<GetSourcesResponse>(await response.Content.ReadAsStreamAsync(ct), cancellationToken: ct);
        return result?.Sources.First().SourceValue.Title;
    }
    
    private class GetSourcesRequest
    {
        [JsonPropertyName("sourceIds")]
        public IEnumerable<long> SourceIds { get; set; }
    }
    
    private class GetSourcesResponse
    {
        [JsonPropertyName("rssSources")]
        public IEnumerable<SourceEntity> Sources { get; set; }

        public class SourceEntity
        {
            [JsonPropertyName("source")]
            public Source SourceValue { get; set; }

            public class Source
            {
                [JsonPropertyName("title")]
                public string Title { get; set; }
            }
        }
    }
}