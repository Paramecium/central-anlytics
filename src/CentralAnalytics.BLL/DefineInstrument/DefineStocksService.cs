﻿using System.Text.Json;
using CentralAnalytics.BLL.Models;
using CentralAnalytics.BLL.Ner;
using CentralAnalytics.BLL.Services;
using CentralAnalytics.DAL.CentralBank;
using CentralAnalytics.DAL.Instrument;

namespace CentralAnalytics.BLL.DefineInstrument;

public interface IDefineStocksService
{
    public Task<IEnumerable<Instrument>> DefineStocksFromArticle(AnalyzedArticleProcessingContext context, CancellationToken ct);
}

public class DefineStocksService : IDefineStocksService
{
    private readonly IInstrumentRepository _instrumentRepository;
    private readonly ITextSimilarityService _similarityService;

    public DefineStocksService(ICentralBankRepository centralBankRepository,
        ITextSimilarityService similarityService, IInstrumentRepository instrumentRepository)
    {
        _similarityService = similarityService;
        _instrumentRepository = instrumentRepository;
    }
    
    public async Task<IEnumerable<Instrument>> DefineStocksFromArticle(AnalyzedArticleProcessingContext context, CancellationToken ct)
    {
        var instruments = (await _instrumentRepository.GetAllActiveInstrumentsFromPostgres(ct)).ToList();
        var instrumentNameEmbeddings = instruments.Select(x => x.Embedding).ToList();
        var recognizedOrganizations = context.RecognizedEntities.Where(item => item.Tag == NamedEntity.Organisation).Select(x => x.Phrase).ToList();
        var recognizedOrganizationEmbeddings = await _similarityService.GetEmbeddingsAsync(recognizedOrganizations, ct);
        if (!recognizedOrganizations.Any())
        {
            return Array.Empty<Instrument>();
        }

        var maxSimilarity = 0.0;
        Instrument maxProbabilityInstrument = null;
        foreach (var organizationEmbedding in recognizedOrganizationEmbeddings)
        {
            instrumentNameEmbeddings.Insert(0, JsonSerializer.Serialize(organizationEmbedding));
            var similarities = (await _similarityService.GetSimilarityByEmbeddingsAsync(instrumentNameEmbeddings, ct)).ToList();
            instrumentNameEmbeddings.RemoveAt(0);
            if (similarities.Max() > maxSimilarity)
            {
                maxSimilarity = similarities.Max();
                var indexOfMax = similarities.IndexOf(maxSimilarity);
                maxProbabilityInstrument = instruments.ElementAt(indexOfMax);
            }
        }

        return maxProbabilityInstrument == null ? Array.Empty<Instrument>() : new List<Instrument>(){maxProbabilityInstrument};
    }
}