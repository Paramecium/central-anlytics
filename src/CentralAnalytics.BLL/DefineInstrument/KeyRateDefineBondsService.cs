﻿using System.Text.Json;
using CentralAnalytics.BLL.Models;
using CentralAnalytics.BLL.Ner;
using CentralAnalytics.BLL.Options;
using CentralAnalytics.BLL.Services;
using CentralAnalytics.DAL.Bonds;
using CentralAnalytics.DAL.CentralBank;
using Microsoft.Extensions.Options;

namespace CentralAnalytics.BLL.DefineInstrument;


public interface IKeyRateDefineBondsService
{
    Task<IEnumerable<Bond>>  DefineBondsFromArticle(AnalyzedArticleProcessingContext context, CancellationToken ct);
}

public class KeyRateDefineBondsService : IKeyRateDefineBondsService
{
    private readonly IOptionsMonitor<DefineInstrumentOptions> _options;
    private readonly ICentralBankRepository _centralBankRepository;
    private readonly IBondsRepository _bondsRepository;
    private readonly ITextSimilarityService _similarityService;

    public KeyRateDefineBondsService(IOptionsMonitor<DefineInstrumentOptions> options,
        ICentralBankRepository centralBankRepository,
        ITextSimilarityService similarityService, 
        IBondsRepository bondsRepository)
    {
        _options = options;
        _centralBankRepository = centralBankRepository;
        _similarityService = similarityService;
        _bondsRepository = bondsRepository;
    }
    
    public async Task<IEnumerable<Bond>> DefineBondsFromArticle(AnalyzedArticleProcessingContext context, CancellationToken ct)
    {
        var bonds = (await _bondsRepository.GetAllBondsFromPostgres(ct)).ToList();
        var countries = bonds.Select(x => x.CountryOfRiskName).Distinct().Where(x => x.Any()).ToList();
        var countriesEmbeddings = bonds.Select(x => x.Embedding).Distinct().Where(x => x.Any()).ToList();
        var recognizedCountries = context.RecognizedEntities.Where(item => item.Tag == NamedEntity.GPE).Select(x => x.Phrase).ToList();
        if (!recognizedCountries.Any())
        {
            recognizedCountries = context.RecognizedEntities.Where(item => item.Tag == NamedEntity.Organisation).Select(x => x.Phrase).ToList();
        }
        var recognizedCountriesEmbeddings = await _similarityService.GetEmbeddingsAsync(recognizedCountries, ct);

        var maxSimilarity = 0.0;
        var maxProbabilityCountry = "";
        foreach (var countryEmbedding in recognizedCountriesEmbeddings)
        {
            countriesEmbeddings.Insert(0, JsonSerializer.Serialize(countryEmbedding));
            var similarities = (await _similarityService.GetSimilarityAsync(countriesEmbeddings, ct)).ToList();
            countriesEmbeddings.RemoveAt(0);
            if (similarities.Max() > maxSimilarity)
            {
                maxSimilarity = similarities.Max();
                var indexOfMax = similarities.IndexOf(maxSimilarity);
                maxProbabilityCountry = countries.ElementAt(indexOfMax);
            }
        }

        return bonds.Where(x => x.CountryOfRiskName.Equals(maxProbabilityCountry) && x.Sector == "government");
    }
}