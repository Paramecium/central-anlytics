﻿using System.Text.Json;
using CentralAnalytics.BLL.Models;
using CentralAnalytics.BLL.Ner;
using CentralAnalytics.BLL.Rule;
using CentralAnalytics.BLL.Services;
using CentralAnalytics.DAL.Decision;
using CentralAnalytics.Shared.Infrastructure.Kafka.KafkaEvent;
using CentralAnalytics.Shared.Infrastructure.Metrics;
using CentralAnalytics.Shared.Models;
using CentralAnalytics.TelegramReports;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace CentralAnalytics.BLL.Kafka.NewsAnalyzedEvent;

public class AnalyzedArticleHandler : IKafkaEventHandler<AnalyzedArticle>
{
    private IRule[][]? _stages;
    private readonly object _initStagesLocker = new ();
    
    private readonly IServiceProvider _serviceProvider;
    private readonly IProcessedMessagesMetrics _processedMessagesMetrics;
    private readonly ILogger<AnalyzedArticleHandler> _logger;
    private readonly TelegramService _telegramService;
    private readonly IDecisionRepository _decisionRepository;
    private readonly IFeedReaderService _feedReaderService;

    public AnalyzedArticleHandler(
        IServiceProvider serviceProvider,
        ILogger<AnalyzedArticleHandler> logger,
        IProcessedMessagesMetrics processedMessagesMetrics,
        TelegramService telegramService,
        IDecisionRepository decisionRepository, 
        IFeedReaderService feedReaderService)
    {
        _serviceProvider = serviceProvider;
        _logger = logger;
        _processedMessagesMetrics = processedMessagesMetrics;
        _telegramService = telegramService;
        _decisionRepository = decisionRepository;
        _feedReaderService = feedReaderService;
    }

    public async Task Handle(AnalyzedArticle message,
        CancellationToken ct = default)
    {
        if (message == null) return;
        var log =
            $"({DateTimeOffset.UtcNow}) Получено сообщение о проанализированной новости:{Environment.NewLine}" +
            $"{JsonSerializer.Serialize(message)}";

        _logger.LogInformation(log);
        
        InitStages();
        _processedMessagesMetrics.ProcessedMessagesCountInc(message);

        if (message.FilterType is FilteredNewsType.Other or FilteredNewsType.Opinion || !message.SecondType.ToString().StartsWith(message.MainType.ToString()))
        {
            return;
        }

        var context = new AnalyzedArticleProcessingContext
        {
            AnalyzedArticle = message,
            RecognizedEntities = BIONerParser.ParseNer(message.Words, message.Tags)
        };

        foreach (var stageRules in _stages!)
        {
            if (!stageRules.Any())
            {
                continue;
            }

            await Task.WhenAll(stageRules.Select(rule =>
                Task.Run(() => rule.ApplyAsync(context, ct), ct)));
        }

        if (context.Result != AnalyzeResult.None)
        {
            var sourceTitle = await _feedReaderService.GetSourceNameById(context.AnalyzedArticle.SourceId, ct);
            await _telegramService.SendMessage(MessageFormatter.MessageFormatter.GetFormattedText(context, sourceTitle), ct);
            await _decisionRepository.InsertDecision(
                new DecisionWithInstruments
                {
                    BondIds = context.PossibleBonds.Select(x => x.Id).Take(10).ToList(),
                    InstrumentIds = context.PossibleInstruments.Select(x => x.Id).ToList(),
                    Text = context.AnalyzedArticle.Summary,
                    Result = (int)context.Result
                }, ct);
        }
    }

    public async Task<Result> AnalyzeMessage(AnalyzedArticle message,
        CancellationToken ct = default)
    {
        if (message == null) return new Result(AnalyzeResult.None, "Incorrect message");
  
        InitStages();

        if (message.FilterType is FilteredNewsType.Other or FilteredNewsType.Opinion || !message.SecondType.ToString().StartsWith(message.MainType.ToString()))
        {
            return new Result(AnalyzeResult.None, "Bad filter class");;
        }

        var context = new AnalyzedArticleProcessingContext
        {
            AnalyzedArticle = message,
            RecognizedEntities = BIONerParser.ParseNer(message.Words, message.Tags)
        };

        foreach (var stageRules in _stages!)
        {
            if (!stageRules.Any())
            {
                continue;
            }

            await Task.WhenAll(stageRules.Select(rule =>
                Task.Run(() => rule.ApplyAsync(context, ct), ct)));
        }

        if (context.Result != AnalyzeResult.None)
        {
            var sourceTitle = "";
            try
            {
                sourceTitle = await _feedReaderService.GetSourceNameById(context.AnalyzedArticle.SourceId, ct);
            }
            catch (Exception e)
            {
                sourceTitle = "Unknown";
            }
            var messageToUser = MessageFormatter.MessageFormatter.GetFormattedText(context, sourceTitle);
            return new Result(context.Result, messageToUser);
        }

        return new Result(AnalyzeResult.None, "News item has no impact on prices");
    }
    
    private IRule[][] GetStages()
    {
        return new[]
        {
            new IRule[]
            {
                _serviceProvider.GetRequiredService<KeyRateRule>(),
                _serviceProvider.GetRequiredService<CorporateReportRule>(),
                _serviceProvider.GetRequiredService<BuyBackRule>(),
                _serviceProvider.GetRequiredService<ExtraEmissionRule>(),
                _serviceProvider.GetRequiredService<DelistingRule>(),
                _serviceProvider.GetRequiredService<CorporateDividendsRule>()
            }
        };
    }

    private void InitStages()
    {
        if (_stages != null)
        {
            return;
        }
            
        lock (_initStagesLocker)
        {
            if (_stages != null)
            {
                return;
            }

            _stages = GetStages();
        }
    }

    public record Result(AnalyzeResult AnalyzeResult, string Message);

}