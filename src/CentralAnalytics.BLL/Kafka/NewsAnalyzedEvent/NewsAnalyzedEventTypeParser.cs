using CentralAnalytics.Shared.Infrastructure.Kafka.KafkaEvent;
using CentralAnalytics.Shared.Models;
using Confluent.Kafka;

namespace CentralAnalytics.BLL.Kafka.NewsAnalyzedEvent;

public class NewsAnalyzedEventTypeParser : IKafkaEventTypeParser
{
    public string ResolveEventType(
        ConsumeResult<Ignore, string> consumeResult) =>
        nameof(AnalyzedArticle);
}
