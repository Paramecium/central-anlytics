using CentralAnalytics.Shared.Infrastructure.Kafka.KafkaEvent;
using Confluent.Kafka;

namespace CentralAnalytics.BLL.Kafka.NewsAnalyzedEvent;

public class AnalyzedArticleConsumerSettings : IKafkaEventsConsumerSettings
{
    public ConsumerConfig? ConsumerConfig { get; set; }

    public int ConsumeTimeoutMilliseconds { get; set; }

    public string? Topic { get; set; }
}
