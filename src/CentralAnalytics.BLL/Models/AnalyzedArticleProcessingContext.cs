﻿using CentralAnalytics.BLL.Kafka.NewsAnalyzedEvent;
using CentralAnalytics.BLL.Ner;
using CentralAnalytics.DAL.Bonds;
using CentralAnalytics.DAL.Instrument;
using CentralAnalytics.Shared.Models;

namespace CentralAnalytics.BLL.Models;

public class AnalyzedArticleProcessingContext
{
    public AnalyzedArticle? AnalyzedArticle { get; set; }

    public List<Instrument> PossibleInstruments { get; set; } = new();

    public List<Bond> PossibleBonds { get; set; } = new();

    public List<TagWithPhrase> RecognizedEntities { get; set; } = new();

    public AnalyzeResult Result { get; set; } = AnalyzeResult.None;
}

public enum AnalyzeResult
{
    None = 0,
    Hold = 1,
    Up = 2,
    Down = 3
}