﻿using Grpc.Core;
using Grpc.Core.Interceptors;
using Grpc.Net.Client;
using Tinkoff.InvestApi.V1;

namespace CentralAnalytics.TinkoffClient;

public class TinkoffClient
{
    private readonly CallInvoker? _callInvoker = GrpcChannel
        .ForAddress("https://sandbox-invest-public-api.tinkoff.ru:443")
        .Intercept();
    
    public TinkoffClient()
    {
        Instruments = new InstrumentsService.InstrumentsServiceClient(_callInvoker);
        MarketData = new MarketDataService.MarketDataServiceClient(_callInvoker);
        Operations = new OperationsService.OperationsServiceClient(_callInvoker);
        Orders = new OrdersService.OrdersServiceClient(_callInvoker);
        Sandbox = new SandboxService.SandboxServiceClient(_callInvoker);
        StopOrders = new StopOrdersService.StopOrdersServiceClient(_callInvoker);
        Users = new UsersService.UsersServiceClient(_callInvoker);

        _marketDataStream = new MarketDataStreamService.MarketDataStreamServiceClient(_callInvoker);
        _ordersStream = new OrdersStreamService.OrdersStreamServiceClient(_callInvoker);
    }

    private const string AccessToken
        = "t.mCtdUYuvxh4TlsWnXvSf1YJJamd4WaPEq8ihWLIizwA--p7kOUtMBjRKkbjoap2Q6m7k8L_F6wumf6-JHDQ-0A";

    public Metadata _headers = new Metadata
    {
        new("Authorization", $"Bearer {AccessToken}")
    };

    public InstrumentsService.InstrumentsServiceClient Instruments { get; }
    public MarketDataService.MarketDataServiceClient MarketData { get; }
    public OperationsService.OperationsServiceClient Operations { get; }
    public OrdersService.OrdersServiceClient Orders { get; }
    public SandboxService.SandboxServiceClient Sandbox { get; }
    public StopOrdersService.StopOrdersServiceClient StopOrders { get; }
    public UsersService.UsersServiceClient Users { get; }

    // Стримы
    private readonly OrdersStreamService.OrdersStreamServiceClient _ordersStream;
    private readonly MarketDataStreamService.MarketDataStreamServiceClient _marketDataStream;
    
    public AsyncServerStreamingCall<MarketDataResponse> MarketDataServerSideStream(
        MarketDataServerSideStreamRequest req,
        CancellationToken ct = default)
        => _marketDataStream.MarketDataServerSideStream(
            req,
            _headers,
            deadline: default,
            ct);

    public AsyncDuplexStreamingCall<MarketDataRequest, MarketDataResponse> MarketDataStream(
        CancellationToken ct = default)
        => _marketDataStream.MarketDataStream(
            _headers,
            deadline: default,
            ct);

    public AsyncServerStreamingCall<TradesStreamResponse> TradesStream(
        TradesStreamRequest req,
        CancellationToken ct = default)
        => _ordersStream.TradesStream(
            req,
            _headers,
            deadline: default,
            ct);

    public BondsResponse GetBonds(InstrumentsRequest request, CancellationToken ct = default) =>
        Instruments.Bonds(request, _headers, deadline: default, ct);
};
