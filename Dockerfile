FROM mcr.microsoft.com/dotnet/sdk:6.0 AS build-env
ARG project_name="CentralAnalytics"
WORKDIR /app

COPY ./src ./
RUN dotnet publish $project_name -c Release -o out

FROM mcr.microsoft.com/dotnet/aspnet:6.0
WORKDIR /app
COPY --from=build-env /app/${project_name}/out .

EXPOSE 80

ENV ASPNETCORE_ENVIRONMENT=Production
ENV ASPNETCORE_FORWARDEDHEADERS_ENABLED=true
ENTRYPOINT ["dotnet", "CentralAnalytics.dll"]
